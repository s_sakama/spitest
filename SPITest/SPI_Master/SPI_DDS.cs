﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPI.SPI_AD9958
{
    /// <summary>
    /// Class for action specific to DDS.
    /// </summary>
    public abstract class SPI_AD9958 : SPI_Master
    {
        
        public double RefCLK { get; protected set; }
        public double SysCLK { get; protected set; }
        protected double[] outputFreq = new double[2];
        public double[] OutputFreq { get { return outputFreq; }}
        /// <summary>
        /// Send commands to initialize DDS chip.
        /// </summary>
        /// <param name="refClk">Reference clock frequency [Hz] </param>
        /// <param name="freqMultiplier"> PLL divider ratio. Value must be from 4 to 20, or PLL will be disabled.</param>
        public abstract void InitializeDDS(double refClk, byte freqMultiplier);

        protected abstract void SetInstructionByte();
        protected abstract void SetFR1(byte freqMultiplier);
        protected abstract void SetFR2();

        public abstract void SetFrequency(double ch0, double ch1);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ch">0 for ch0, 1 for ch1</param>
        /// <param name="freq">desired frequency</param>
        protected abstract void SetFreqCh(byte ch, double freq);
    }
}
