﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPI
{

    /// <summary>
    /// SPIBitOrder
    /// </summary>
    public enum SPIMode
    {
        mode0,
        mode1,
        mode2,
        mode3
    }

    public enum SPIBitOrder
    {
        MSBFirst,
        LSBFirst
    }

    public enum SPIClockPhase
    {
        // Data is capture on rising edge.
        Rising,
        // Data is captured on falling edge.
        Falling
    }

    public class SPIConfig
    {
        // true for positive logic, false for negative logic
        public SPIMode mode;
        public SPIBitOrder bitOrder;
    }


    public abstract class SPI_Master
    {

        public SPIMode Mode { get; protected set; }

        /// <summary>
        /// Returns SPIMode based on clock polarity (CPOL) and clock phase (CPHA).
        /// </summary>
        /// <param name="cpol">Clock polarity, true for idle state high, false for idle state low. </param>
        /// <param name="cpha">Clock phase, true if data are sampled on the second clock edge, false if data are sampled on the first clock edge.</param>
        /// <returns></returns>
        public static SPIMode SPIModeBuilder(bool cpol, bool cpha)
        {
            SPIMode spiMode;
            if (cpol)
            {
                if (cpha)
                {
                    spiMode = SPIMode.mode3;
                }
                else
                {
                    spiMode = SPIMode.mode2;
                }
            }
            else
            {
                if (cpha)
                {
                    spiMode = SPIMode.mode1;
                }
                else
                {
                    spiMode = SPIMode.mode0;
                }
            }
            return spiMode;
        }

        public abstract void InitChannel(SPIConfig conf);

        public abstract void CloseChannel();

        public abstract void Read();

        public abstract void Write(byte[] dataToTransfer, int bytesToTransfer, ref uint bytesTransfered);
    }
}
