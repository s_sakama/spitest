﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using SPI;
using FTD2XX_SPI;
using FTD2XX_SPI.SPIException;

using NationalInstruments.DAQmx;
using NITask = NationalInstruments.DAQmx.Task;

namespace SPITest
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private FTDI_SPI ftdiSPI;
        private bool dev_checked = false;
        private Stopwatch sw = new Stopwatch();

        private NITask niTask;

        private void CheckDevice(object sender, RoutedEventArgs e)
        {
            try
            {
                ftdiSPI = new FTDI_SPI();
                dev_checked = true;
                MessageBox.Show("Device Check Completed");
            }
            catch(Exception err)
            {
                dev_checked = false;
                MessageBox.Show(err.ToString());
            }
        }

        private const float samplingRate = 10000000;

        /*private void SPITransferTest(object sender, RoutedEventArgs e)
        {
            if(ftdiSPI == null)
            {
                try
                {
                    ftdiSPI = new FTDI_SPI();
                    dev_checked = true;
                }
                catch (Exception err)
                {
                    dev_checked = false;
                    MessageBox.Show(err.ToString());
                }
            }

            try
            {
                using (niTask = new NITask())
                {
                    niTask.DOChannels.CreateChannel("Dev1/port0", "port0", ChannelLineGrouping.OneChannelForAllLines);
                    
                    niTask.Timing.ConfigureSampleClock("", samplingRate, SampleClockActiveEdge.Rising, SampleQuantityMode.ContinuousSamples);
                    DigitalSingleChannelWriter writer = new DigitalSingleChannelWriter(niTask.Stream);
                    niTask.Control(TaskAction.Verify);
                    writer.WriteSingleSamplePort(false, 0xFF);

                    //niTask.Start();

                    byte[] dataToSend = new byte[500];
                    uint dataTransfered = 0;
                    SPIConfig conf = new SPIConfig();
                    conf.bitOrder = SPIBitOrder.MSBFirst;
                    //conf.clockPhase = SPIClockPhase.Rising;
                    conf.mode = SPIMode.mode0;
                    //conf.clockPolarity = true;
                    // ConfigureMPSSE() causes strange edge.
                    //ftdiSPI.ConfigureMPSSE();
                    ftdiSPI.InitChannel(conf);
                    for(int i = 0; i< 200; i++)
                    {
                        dataToSend[i] = 0xAA;
                    }

                    //writer.WriteSingleSamplePort(false, 0xFF);

                    sw.Start();
                    // apply voltage here.
                    niTask.Start();
                    ftdiSPI.Write(dataToSend, 1, ref dataTransfered);

                    //writer.WriteSingleSamplePort(true, 0xAA);
                    sw.Stop();
                    
                    MessageBox.Show("SPI Transfer Test Complete. \r\nElapsedTime : " + sw.ElapsedMilliseconds + "[ms]\r\nFrequency : " + Stopwatch.Frequency + "\r\n" + "Hi-res Enabled : " + Stopwatch.IsHighResolution + "\r\n"+dataTransfered+" Bytes sent\r\n"+"OPCODE : "+ftdiSPI.OPCODE_W);
                    niTask.Stop();

                }
            } catch(DaqException err)
            {
                MessageBox.Show(err.ToString());
            }
        }*/
        protected virtual void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MessageBoxResult.Yes != MessageBox.Show("Are you sure you want to exit?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Information))
            {
                e.Cancel = true;
                return;
            }
            else
            {
                ftdiSPI.Dispose();
                return;
            }

        }

        /*private void BlinkTest(object sender, RoutedEventArgs e)
        {
            if (ftdiSPI == null)
            {
                try
                {
                    ftdiSPI = new FTDI_SPI();
                    dev_checked = true;
                }
                catch (Exception err)
                {
                    dev_checked = false;
                    MessageBox.Show(err.ToString());
                }
            }

            ftdiSPI.Blink(int.Parse(BlinkTimes.Text), int.Parse(BlinkInterval.Text));
        }*/

        private void SPITransferTestSimple(object sender, RoutedEventArgs e)
        {
            if(ftdiSPI == null)
            {
                ftdiSPI = new FTDI_SPI();
            }
            ftdiSPI.SPITestFunction();
        }

        private bool isSPIChannelInitialized = false;
        private void GenerateSignal(object sender, RoutedEventArgs e)
        {
            SPIConfig conf = new SPIConfig();
            conf.bitOrder = SPIBitOrder.MSBFirst;
            conf.mode = SPIMode.mode2;

            if (ftdiSPI == null)
            {
                ftdiSPI = new FTDI_SPI();
            }

            if (!isSPIChannelInitialized)
            {
                ftdiSPI.InitChannel(conf);
                isSPIChannelInitialized = true;
            }

            ftdiSPI.InitializeDDS(30000000, 4);

            ftdiSPI.SetFrequency(1000000, 2000000);

        }
    }
}
