﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using SuppressUnmanagedCodeSecurityAttribute = System.Security.SuppressUnmanagedCodeSecurityAttribute;

namespace AndorSDK {
    /// <summary>
    /// This is PInvoke wrapper generated from Andor SDK 3/atcore.h and Andor SDK 3/atutility.h
    /// by 'SigImp Translate Snippet' feature of P/Invoke Interop Assistant 1.0
    /// To use these function, dll of Andor SDK 3 is needed
    /// </summary>
    public static class ATCore {

        /// ATCORE_H -> 
        /// Error generating expression: Value cannot be null.
        ///Parameter name: node
        public const string ATCORE_H = "";

        /// AT_EXP_CONV -> WINAPI
        public const string AT_EXP_CONV = WINAPI;

        /// AT_INFINITE -> 0xFFFFFFFF
        public const int AT_INFINITE = -1;

        /// AT_CALLBACK_SUCCESS -> 0
        public const int AT_CALLBACK_SUCCESS = 0;

        /// AT_TRUE -> 1
        public const int AT_TRUE = 1;

        /// AT_FALSE -> 0
        public const int AT_FALSE = 0;

        /// AT_SUCCESS -> 0
        public const int AT_SUCCESS = 0;

        /// AT_ERR_NOTINITIALISED -> 1
        public const int AT_ERR_NOTINITIALISED = 1;

        /// AT_ERR_NOTIMPLEMENTED -> 2
        public const int AT_ERR_NOTIMPLEMENTED = 2;

        /// AT_ERR_READONLY -> 3
        public const int AT_ERR_READONLY = 3;

        /// AT_ERR_NOTREADABLE -> 4
        public const int AT_ERR_NOTREADABLE = 4;

        /// AT_ERR_NOTWRITABLE -> 5
        public const int AT_ERR_NOTWRITABLE = 5;

        /// AT_ERR_OUTOFRANGE -> 6
        public const int AT_ERR_OUTOFRANGE = 6;

        /// AT_ERR_INDEXNOTAVAILABLE -> 7
        public const int AT_ERR_INDEXNOTAVAILABLE = 7;

        /// AT_ERR_INDEXNOTIMPLEMENTED -> 8
        public const int AT_ERR_INDEXNOTIMPLEMENTED = 8;

        /// AT_ERR_EXCEEDEDMAXSTRINGLENGTH -> 9
        public const int AT_ERR_EXCEEDEDMAXSTRINGLENGTH = 9;

        /// AT_ERR_CONNECTION -> 10
        public const int AT_ERR_CONNECTION = 10;

        /// AT_ERR_NODATA -> 11
        public const int AT_ERR_NODATA = 11;

        /// AT_ERR_INVALIDHANDLE -> 12
        public const int AT_ERR_INVALIDHANDLE = 12;

        /// AT_ERR_TIMEDOUT -> 13
        public const int AT_ERR_TIMEDOUT = 13;

        /// AT_ERR_BUFFERFULL -> 14
        public const int AT_ERR_BUFFERFULL = 14;

        /// AT_ERR_INVALIDSIZE -> 15
        public const int AT_ERR_INVALIDSIZE = 15;

        /// AT_ERR_INVALIDALIGNMENT -> 16
        public const int AT_ERR_INVALIDALIGNMENT = 16;

        /// AT_ERR_COMM -> 17
        public const int AT_ERR_COMM = 17;

        /// AT_ERR_STRINGNOTAVAILABLE -> 18
        public const int AT_ERR_STRINGNOTAVAILABLE = 18;

        /// AT_ERR_STRINGNOTIMPLEMENTED -> 19
        public const int AT_ERR_STRINGNOTIMPLEMENTED = 19;

        /// AT_ERR_NULL_FEATURE -> 20
        public const int AT_ERR_NULL_FEATURE = 20;

        /// AT_ERR_NULL_HANDLE -> 21
        public const int AT_ERR_NULL_HANDLE = 21;

        /// AT_ERR_NULL_IMPLEMENTED_VAR -> 22
        public const int AT_ERR_NULL_IMPLEMENTED_VAR = 22;

        /// AT_ERR_NULL_READABLE_VAR -> 23
        public const int AT_ERR_NULL_READABLE_VAR = 23;

        /// AT_ERR_NULL_READONLY_VAR -> 24
        public const int AT_ERR_NULL_READONLY_VAR = 24;

        /// AT_ERR_NULL_WRITABLE_VAR -> 25
        public const int AT_ERR_NULL_WRITABLE_VAR = 25;

        /// AT_ERR_NULL_MINVALUE -> 26
        public const int AT_ERR_NULL_MINVALUE = 26;

        /// AT_ERR_NULL_MAXVALUE -> 27
        public const int AT_ERR_NULL_MAXVALUE = 27;

        /// AT_ERR_NULL_VALUE -> 28
        public const int AT_ERR_NULL_VALUE = 28;

        /// AT_ERR_NULL_STRING -> 29
        public const int AT_ERR_NULL_STRING = 29;

        /// AT_ERR_NULL_COUNT_VAR -> 30
        public const int AT_ERR_NULL_COUNT_VAR = 30;

        /// AT_ERR_NULL_ISAVAILABLE_VAR -> 31
        public const int AT_ERR_NULL_ISAVAILABLE_VAR = 31;

        /// AT_ERR_NULL_MAXSTRINGLENGTH -> 32
        public const int AT_ERR_NULL_MAXSTRINGLENGTH = 32;

        /// AT_ERR_NULL_EVCALLBACK -> 33
        public const int AT_ERR_NULL_EVCALLBACK = 33;

        /// AT_ERR_NULL_QUEUE_PTR -> 34
        public const int AT_ERR_NULL_QUEUE_PTR = 34;

        /// AT_ERR_NULL_WAIT_PTR -> 35
        public const int AT_ERR_NULL_WAIT_PTR = 35;

        /// AT_ERR_NULL_PTRSIZE -> 36
        public const int AT_ERR_NULL_PTRSIZE = 36;

        /// AT_ERR_NOMEMORY -> 37
        public const int AT_ERR_NOMEMORY = 37;

        /// AT_ERR_DEVICEINUSE -> 38
        public const int AT_ERR_DEVICEINUSE = 38;

        /// AT_ERR_DEVICENOTFOUND -> 39
        public const int AT_ERR_DEVICENOTFOUND = 39;

        /// AT_ERR_HARDWARE_OVERFLOW -> 100
        public const int AT_ERR_HARDWARE_OVERFLOW = 100;

        /// AT_HANDLE_UNINITIALISED -> -1
        public const int AT_HANDLE_UNINITIALISED = -1;

        /// AT_HANDLE_SYSTEM -> 1
        public const int AT_HANDLE_SYSTEM = 1;

        /// WINAPI -> __winapi
        /// Error generating expression: Value __winapi is not resolved
        public const string WINAPI = "__winapi";

        /// Return Type: int
        [DllImport("atcore.dll", EntryPoint = "AT_InitialiseLibrary")]
        public static extern int AT_InitialiseLibrary();


        /// Return Type: int
        [DllImport("atcore.dll", EntryPoint = "AT_FinaliseLibrary")]
        public static extern int AT_FinaliseLibrary();


        /// Return Type: int
        ///CameraIndex: int
        ///Hndl: AT_H*
        [DllImport("atcore.dll", EntryPoint = "AT_Open")]
        public static extern int AT_Open(int CameraIndex, ref int Hndl);


        /// Return Type: int
        ///Hndl: AT_H->int
        [DllImport("atcore.dll", EntryPoint = "AT_Close")]
        public static extern int AT_Close(int Hndl);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///EvCallback: FeatureCallback
        ///Context: void*
        [DllImport("atcore.dll", EntryPoint = "AT_RegisterFeatureCallback")]
        public static extern int AT_RegisterFeatureCallback(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, FeatureCallback EvCallback, IntPtr Context);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///EvCallback: FeatureCallback
        ///Context: void*
        [DllImport("atcore.dll", EntryPoint = "AT_UnregisterFeatureCallback")]
        public static extern int AT_UnregisterFeatureCallback(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, FeatureCallback EvCallback, IntPtr Context);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///Implemented: AT_BOOL*
        [DllImport("atcore.dll", EntryPoint = "AT_IsImplemented")]
        public static extern int AT_IsImplemented(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, ref int Implemented);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///Readable: AT_BOOL*
        [DllImport("atcore.dll", EntryPoint = "AT_IsReadable")]
        public static extern int AT_IsReadable(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, ref int Readable);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///Writable: AT_BOOL*
        [DllImport("atcore.dll", EntryPoint = "AT_IsWritable")]
        public static extern int AT_IsWritable(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, ref int Writable);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///ReadOnly: AT_BOOL*
        [DllImport("atcore.dll", EntryPoint = "AT_IsReadOnly")]
        public static extern int AT_IsReadOnly(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, ref int ReadOnly);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///Value: AT_64->int
        //[DllImport("atcore.dll", EntryPoint = "AT_SetInt")]
        //public static extern int AT_SetInt(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, int Value);
        [DllImport("atcore.dll", EntryPoint = "AT_SetInt")]
        public static extern int AT_SetInt(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, long Value);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///Value: AT_64*
        [DllImport("atcore.dll", EntryPoint = "AT_GetInt")]
        public static extern int AT_GetInt(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, ref long Value);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///MaxValue: AT_64*
        [DllImport("atcore.dll", EntryPoint = "AT_GetIntMax")]
        public static extern int AT_GetIntMax(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, ref long MaxValue);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///MinValue: AT_64*
        [DllImport("atcore.dll", EntryPoint = "AT_GetIntMin")]
        public static extern int AT_GetIntMin(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, ref long MinValue);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///Value: double
        [DllImport("atcore.dll", EntryPoint = "AT_SetFloat")]
        public static extern int AT_SetFloat(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, double Value);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///Value: double*
        [DllImport("atcore.dll", EntryPoint = "AT_GetFloat")]
        public static extern int AT_GetFloat(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, ref double Value);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///MaxValue: double*
        [DllImport("atcore.dll", EntryPoint = "AT_GetFloatMax")]
        public static extern int AT_GetFloatMax(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, ref double MaxValue);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///MinValue: double*
        [DllImport("atcore.dll", EntryPoint = "AT_GetFloatMin")]
        public static extern int AT_GetFloatMin(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, ref double MinValue);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///Value: AT_BOOL->int
        [DllImport("atcore.dll", EntryPoint = "AT_SetBool")]
        public static extern int AT_SetBool(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, int Value);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///Value: AT_BOOL*
        [DllImport("atcore.dll", EntryPoint = "AT_GetBool")]
        public static extern int AT_GetBool(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, ref int Value);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///Value: int
        [DllImport("atcore.dll", EntryPoint = "AT_SetEnumerated")]
        public static extern int AT_SetEnumerated(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, int Value);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///String: AT_WC*
        [DllImport("atcore.dll", EntryPoint = "AT_SetEnumeratedString")]
        public static extern int AT_SetEnumeratedString(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string targetStr);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///Value: int*
        [DllImport("atcore.dll", EntryPoint = "AT_GetEnumerated")]
        public static extern int AT_GetEnumerated(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, ref int Value);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///Count: int*
        [DllImport("atcore.dll", EntryPoint = "AT_GetEnumeratedCount")]
        public static extern int AT_GetEnumeratedCount(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, ref int Count);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///Index: int
        ///Available: AT_BOOL*
        [DllImport("atcore.dll", EntryPoint = "AT_IsEnumeratedIndexAvailable")]
        public static extern int AT_IsEnumeratedIndexAvailable(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, int Index, ref int Available);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///Index: int
        ///Implemented: AT_BOOL*
        [DllImport("atcore.dll", EntryPoint = "AT_IsEnumeratedIndexImplemented")]
        public static extern int AT_IsEnumeratedIndexImplemented(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, int Index, ref int Implemented);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///Index: int
        ///String: AT_WC*
        ///StringLength: int
        [DllImport("atcore.dll", EntryPoint = "AT_GetEnumeratedString")]
        public static extern int AT_GetEnumeratedString(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, int Index, IntPtr targetStr, int StringLength);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///Value: int
        [DllImport("atcore.dll", EntryPoint = "AT_SetEnumIndex")]
        public static extern int AT_SetEnumIndex(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, int Value);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///String: AT_WC*
        [DllImport("atcore.dll", EntryPoint = "AT_SetEnumString")]
        public static extern int AT_SetEnumString(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string targetStr);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///Value: int*
        [DllImport("atcore.dll", EntryPoint = "AT_GetEnumIndex")]
        public static extern int AT_GetEnumIndex(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, ref int Value);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///Count: int*
        [DllImport("atcore.dll", EntryPoint = "AT_GetEnumCount")]
        public static extern int AT_GetEnumCount(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, ref int Count);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///Index: int
        ///Available: AT_BOOL*
        [DllImport("atcore.dll", EntryPoint = "AT_IsEnumIndexAvailable")]
        public static extern int AT_IsEnumIndexAvailable(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, int Index, ref int Available);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///Index: int
        ///Implemented: AT_BOOL*
        [DllImport("atcore.dll", EntryPoint = "AT_IsEnumIndexImplemented")]
        public static extern int AT_IsEnumIndexImplemented(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, int Index, ref int Implemented);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///Index: int
        ///String: AT_WC*
        ///StringLength: int
        [DllImport("atcore.dll", EntryPoint = "AT_GetEnumStringByIndex")]
        public static extern int AT_GetEnumStringByIndex(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, int Index, IntPtr targetStr, int StringLength);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        [DllImport("atcore.dll", EntryPoint = "AT_Command")]
        public static extern int AT_Command(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///String: AT_WC*
        [DllImport("atcore.dll", EntryPoint = "AT_SetString")]
        public static extern int AT_SetString(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string targetStr);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC* 
        ///String: AT_WC* (wchar_t)
        ///StringLength: int
        [DllImportAttribute("atcore.dll", EntryPoint = "AT_GetString")]
        public static extern int AT_GetString(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, System.IntPtr String, int StringLength);
        
        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///MaxStringLength: int*
        [DllImport("atcore.dll", EntryPoint = "AT_GetStringMaxLength")]
        public static extern int AT_GetStringMaxLength(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, ref int MaxStringLength);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Ptr: AT_U8*
        ///PtrSize: int
        [SuppressUnmanagedCodeSecurity] // use this to improve speed
        [DllImport("atcore.dll", EntryPoint = "AT_QueueBuffer")]
        public static extern int AT_QueueBuffer(int Hndl, IntPtr Ptr, int PtrSize);
        //[DllImport("atcore.dll", EntryPoint = "AT_QueueBuffer")]
        //public static extern int AT_QueueBuffer(int Hndl, byte[] buffer, int PtrSize);


        /// Return Type: int
        ///Hndl: AT_H->int
        ///Ptr: AT_U8**
        ///PtrSize: int*
        ///Timeout: unsigned int
        [SuppressUnmanagedCodeSecurity] //use this to improve speed
        [DllImport("atcore.dll", EntryPoint = "AT_WaitBuffer")]
        public static extern int AT_WaitBuffer(int Hndl, ref IntPtr Ptr, ref int PtrSize, uint Timeout);
        

        /// Return Type: int
        ///Hndl: AT_H->int
        [DllImport("atcore.dll", EntryPoint = "AT_Flush")]
        public static extern int AT_Flush(int Hndl);

        /// Return Type: int
        ///Hndl: AT_H->int
        ///Feature: AT_WC*
        ///Context: void*
        public delegate int FeatureCallback(int Hndl, [InAttribute()] [MarshalAsAttribute(UnmanagedType.LPWStr)] string Feature, IntPtr Context);


        /// Return Type: int
        ///inputBuffer: AT_U8*
        ///outputBuffer: AT_U8*
        ///width: AT_64->int
        ///height: AT_64->int
        ///stride: AT_64->int
        ///inputPixelEncoding: AT_WC*
        ///outputPixelEncoding: AT_WC*
        //[SuppressUnmanagedCodeSecurity] //use this to improve speed
        //[DllImport("atutility.dll", EntryPoint = "AT_ConvertBuffer")]
        //public static extern int AT_ConvertBuffer(System.IntPtr inputBuffer, System.IntPtr outputBuffer, int width, int height, int stride, [System.Runtime.InteropServices.InAttribute()] [System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.LPWStr)] string inputPixelEncoding, [System.Runtime.InteropServices.InAttribute()] [System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.LPWStr)] string outputPixelEncoding);
        [SuppressUnmanagedCodeSecurity] //use this to improve speed
        [DllImport("atutility.dll", EntryPoint = "AT_ConvertBuffer")]
        public static extern int AT_ConvertBuffer(System.IntPtr inputBuffer, System.IntPtr outputBuffer, long width, long height, long stride, [System.Runtime.InteropServices.InAttribute()] [System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.LPWStr)] string inputPixelEncoding, [System.Runtime.InteropServices.InAttribute()] [System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.LPWStr)] string outputPixelEncoding);


        /// Return Type: int
        ///inputBuffer: AT_U8*
        ///outputBuffer: AT_U8*
        ///imagesizebytes: AT_64->int
        ///outputPixelEncoding: AT_WC*
        //[DllImport("atutility.dll", EntryPoint = "AT_ConvertBufferUsingMetadata")]
        //public static extern int AT_ConvertBufferUsingMetadata(System.IntPtr inputBuffer, System.IntPtr outputBuffer, int imagesizebytes, [System.Runtime.InteropServices.InAttribute()] [System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.LPWStr)] string outputPixelEncoding);
        [DllImport("atutility.dll", EntryPoint = "AT_ConvertBufferUsingMetadata")]
        public static extern int AT_ConvertBufferUsingMetadata(System.IntPtr inputBuffer, System.IntPtr outputBuffer, long imagesizebytes, [System.Runtime.InteropServices.InAttribute()] [System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.LPWStr)] string outputPixelEncoding);

        /// Return Type: int
        [DllImport("atutility.dll", EntryPoint = "AT_InitialiseUtilityLibrary")]
        public static extern int AT_InitialiseUtilityLibrary();


        /// Return Type: int
        [DllImport("atutility.dll", EntryPoint = "AT_FinaliseUtilityLibrary")]
        public static extern int AT_FinaliseUtilityLibrary();




        

        static ATCore() {
            string dllPath;
            if (IntPtr.Size == 4) { //x86
                dllPath = System.IO.Path.Combine(GetExePath(),
                    "AndorDLLx86");
            }
            else {
                dllPath = System.IO.Path.Combine(GetExePath(),
                    "AndorDLLx64");
            }
            AddEnvironmentPaths(new string[] { dllPath });
        }

        private static void AddEnvironmentPaths(IEnumerable<string> paths) {
            var path = new[] { Environment.GetEnvironmentVariable("PATH") ?? string.Empty };

            string newPath = string.Join(
                System.IO.Path.PathSeparator.ToString(), path.Concat(paths));

            Environment.SetEnvironmentVariable("PATH", newPath);
        }

        private static string GetExePath() {
            return System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetEntryAssembly().Location);
        }


    }
}
