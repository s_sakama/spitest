﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace AndorSDK {
    public class UnmanagedAlignedBuffers {
        private int numberOfBuffers;
        private int cameraHandle;
        private int queueIndex = 0;
        private int imageByteCount;
        //private List<IntPtr> ListAcqBuffers;
        //private List<IntPtr> ListAlignedBuffers;
        private IntPtr[] ListAcqBuffers;
        private IntPtr[] ListAlignedBuffers;

        public UnmanagedAlignedBuffers(int numberOfBuffers, int cameraHandle, int imageByteCount) {
            this.numberOfBuffers = numberOfBuffers;
            this.cameraHandle = cameraHandle;
            this.imageByteCount = imageByteCount;
            
            Allocate();
            QueueAll();
        }

        private void Allocate() {
            ListAcqBuffers = new IntPtr[numberOfBuffers];
            ListAlignedBuffers = new IntPtr[numberOfBuffers];

            for (int i=0; i<numberOfBuffers; i++) {
                //IntPtr newAcqBuffer = Marshal.AllocHGlobal(imageByteCount + 7);
                //ListAcqBuffers.Add(newAcqBuffer);
                //IntPtr newAlignedBuffer = new IntPtr((newAcqBuffer + 7).ToInt64() & (~(7L)));
                //ListAlignedBuffers.Add(newAlignedBuffer);
                ListAcqBuffers[i] = Marshal.AllocHGlobal(imageByteCount + 7);
                ListAlignedBuffers[i] = new IntPtr((ListAcqBuffers[i] + 7).ToInt64() & (~(7L)));
            }
        }

        private void QueueAll() {
            for (int i = 0; i < numberOfBuffers; i++) {
                ATCore.AT_QueueBuffer(cameraHandle, ListAlignedBuffers[i], imageByteCount);
            }
        }

        public void QueueNextBuffer() {
            int targetIndex = queueIndex % numberOfBuffers;
            ATCore.AT_QueueBuffer(cameraHandle, ListAlignedBuffers[targetIndex], imageByteCount);
            queueIndex += 1;
        }

        public void Release() {
            for (int i = 0; i < numberOfBuffers; i++) {
                Marshal.FreeHGlobal(ListAcqBuffers[i]);
            }

            //ListAcqBuffers.Clear();
            //ListAlignedBuffers.Clear();
        }

    }
}
