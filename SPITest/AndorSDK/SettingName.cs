﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AndorSDK {
    public static class SettingName {

        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: Sets the number of images that should be summed to obtain each image in sequence.</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string AccumulateCount = "AccumulateCount";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: Dynamically incrementing count during an image sequence.</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string AcquiredCount = "AcquiredCount";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: Configures whether the sensor will be read out in alternating directions. See SensorReadDirection.</para>
        /// <para>Availability: Zyla</para>
        /// </summary>
        public const string AlternatingReadoutDirection = "AlternatingReadoutDirection";


        /// <summary>
        /// <para>Setting Type: Enumerated</para>
        /// <para>Description: Sets up pixel binning on the camera. Options: �1x1 �2x2 �3x3 �4x4 �8x8 See Section 4.6 Area of Interest.</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string AOIBinning = "AOIBinning";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: Configures the Horizontal Binning of the sensor area of interest. See Section 4.6 Area of Interest.</para>
        /// <para>Availability: Simcam, Apogee, Zyla</para>
        /// </summary>
        public const string AOIHBin = "AOIHBin";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: Configures the Height of the sensor area of interest in super- pixels. See Section 4.6 Area of Interest.</para>
        /// <para>Availability: Simcam,Neo,Zyla,Apogee</para>
        /// </summary>
        public const string AOIHeight = "AOIHeight";


        /// <summary>
        /// <para>Setting Type: Enumerated</para>
        /// <para>Description: Options: �Image �Kinetics �TDI �Multitrack</para>
        /// <para>Availability: Apogee, Zyla</para>
        /// </summary>
        public const string AOILayout = "AOILayout";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: Configures the left hand coordinate of the sensor area of interest in sensor pixels. See Section 4.6 Area of Interest.</para>
        /// <para>Availability: Simcam,Neo,Zyla,Apogee</para>
        /// </summary>
        public const string AOILeft = "AOILeft";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: The size of one row in the image in bytes. Extra padding bytes may be added to the end of each line after pixel data to comply with line size granularity restrictions imposed by the underlying hardware interface. See Section 4.3 Image Format.</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string AOIStride = "AOIStride";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: Configures the top coordinate of the sensor area of interest in sensor pixels. See Section 4.6 Area of Interest.</para>
        /// <para>Availability: Simcam,Neo,Zyla,Apogee</para>
        /// </summary>
        public const string AOITop = "AOITop";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: Configures the Vertical Binning of the sensor area of interest. See Section 4.6 Area of Interest.</para>
        /// <para>Availability: Simcam, Apogee, Zyla</para>
        /// </summary>
        public const string AOIVBin = "AOIVBin";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: Configures the Width of the sensor area of interest in super- pixels. See Section 4.6 Area of Interest.</para>
        /// <para>Availability: Simcam,Neo,Zyla,Apogee</para>
        /// </summary>
        public const string AOIWidth = "AOIWidth";


        /// <summary>
        /// <para>Setting Type: Enumerated</para>
        /// <para>Description: Configures which signal appears on the auxiliary output pin.  Options: �FireRow1 �FireRowN �FireAll �FireAny</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string AuxiliaryOutSource = "AuxiliaryOutSource";


        /// <summary>
        /// <para>Setting Type: Enumerated</para>
        /// <para>Description: AuxOutSourceTwo is a configurable output available to the user on the D-type.  Options: �ExternalShutterControl �FrameClock �RowClock �ExposedRowClock</para>
        /// <para>Availability: Zyla</para>
        /// </summary>
        public const string AuxOutSourceTwo = "AuxOutSourceTwo";


        /// <summary>
        /// <para>Setting Type: Float</para>
        /// <para>Description: The Backoff temperature offset of the cooler subsystem.</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string BackoffTemperatureOffset = "BackoffTemperatureOffset";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: Returns the baseline level of the image with current settings</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string Baseline = "Baseline";


        /// <summary>
        /// <para>Setting Type: Enumerated</para>
        /// <para>Description: Returns the number bits used to store information about each pixel of the image. .  Supported Bit Depth will be dependent on the camera. Options Neo/Zyla: �11 Bit or 12 Bit 16 Bit Options Apogee: �12 Bit (Not available AltaF/Aspen/Ascent) �16 Bit  For AltaU/E this is determined by PixelReadoutRate: �Normal -> 16-bit �Fast   -> 12-bit</para>
        /// <para>Availability: Neo,Zyla,Apogee</para>
        /// </summary>
        public const string BitDepth = "BitDepth";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: When enabled this will notify the user that the image buffer on the camera has been exceeded, causing the current acquisition to stop.</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string BufferOverflowEvent = "BufferOverflowEvent";


        /// <summary>
        /// <para>Setting Type: Float</para>
        /// <para>Description: Returns the calculated bytes per pixel.  This is read only.</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string BytesPerPixel = "BytesPerPixel";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: Returns whether or not an acquisition is currently acquiring.</para>
        /// <para>Availability: Simcam, Neo, Zyla</para>
        /// </summary>
        public const string CameraAcquiring = "CameraAcquiring";


        /// <summary>
        /// <para>Setting Type: String</para>
        /// <para>Description: Returns the family of the camera.</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string CameraFamily = "CameraFamily";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: Returns the amount of available memory for storing images, in bytes.</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string CameraMemory = "CameraMemory";


        /// <summary>
        /// <para>Setting Type: String</para>
        /// <para>Description: Returns the camera model.</para>
        /// <para>Availability: Simcam, Neo, Zyla</para>
        /// </summary>
        public const string CameraModel = "CameraModel";


        /// <summary>
        /// <para>Setting Type: String</para>
        /// <para>Description: Returns the name of the camera.</para>
        /// <para>Availability: Neo,Zyla,Apogee</para>
        /// </summary>
        public const string CameraName = "CameraName";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: Returns whether the camera is connected to the system. Register a callback to this feature to be notified if the camera is disconnected. Notification of disconnection will not occur if CameraAcquiring is true, in this case AT_WaitBuffer will return an error.</para>
        /// <para>Availability: any</para>
        /// </summary>
        public const string CameraPresent = "CameraPresent";


        /// <summary>
        /// <para>Setting Type: Enumerated</para>
        /// <para>Description: Controls which colour filter is enabled.  Options: �None �Bayer �TRUESENSE</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string ColourFilter = "ColourFilter";


        /// <summary>
        /// <para>Setting Type: String</para>
        /// <para>Description: Returns a unique identifier for the camera controller device. i.e. Frame grabber over Cameralink</para>
        /// <para>Availability: Neo(CL),Zyla(CL)</para>
        /// </summary>
        public const string ControllerID = "ControllerID";


        /// <summary>
        /// <para>Setting Type: Float</para>
        /// <para>Description: Percentage of maximum power being used by the cooler.</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string CoolerPower = "CoolerPower";


        /// <summary>
        /// <para>Setting Type: Enumerated</para>
        /// <para>Description: Configures whether the camera will acquire a fixed length  sequence or a continuous sequence.  In Fixed mode the camera will acquire ‘FrameCount’ number of images and then stop automatically. In Continuous mode the camera will continue to acquire images indefinitely until the ‘AcquisitionStop’ command is issued.  Options: �Fixed �Continuous</para>
        /// <para>Availability: Simcam, Neo, Zyla</para>
        /// </summary>
        public const string CycleMode = "CycleMode";


        /// <summary>
        /// <para>Setting Type: String</para>
        /// <para>Description: Returns the amount of DDR2 Memory installed.</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string DDR2Type = "DDR2Type";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: Returns the number of cameras detected.</para>
        /// <para>Availability: System</para>
        /// </summary>
        public const string DeviceCount = "DeviceCount";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: Returns the /dev/videoN number in Linux only</para>
        /// <para>Availability: Neo(CL),Zyla(CL)</para>
        /// </summary>
        public const string DeviceVideoIndex = "DeviceVideoIndex";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: Enable or Disable shutter during acquisition. Overridden by ForceShutterOpen.</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string DisableShutter = "DisableShutter";


        /// <summary>
        /// <para>Setting Type: String</para>
        /// <para>Description: Returns USB driver version if USB Interface. Returns cURL library version if Ethernet interface.</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string DriverVersion = "DriverVersion";


        /// <summary>
        /// <para>Setting Type: Enumerated</para>
        /// <para>Description: Configures which on-sensor electronic shuttering mode is used. �For pulsed or fast moving images Global shuttering is recommended. �For the highest frame rates and best noise performance Rolling is recommended.  Options: �Rolling �Global</para>
        /// <para>Availability: Simcam, Neo, Zyla</para>
        /// </summary>
        public const string ElectronicShutteringMode = "ElectronicShutteringMode";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: Enable or Disable the acquisition event selected via the EventSelector feature.</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string EventEnable = "EventEnable";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: When enabled this will notify the user that an acquisition event, which the user registered a callback for, has been missed.</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string EventsMissedEvent = "EventsMissedEvent";


        /// <summary>
        /// <para>Setting Type: Enumerated</para>
        /// <para>Description: Selects the acquisition events you wish to enable or disable using the EventEnable feature.  Options: �ExposureEndEvent �ExposureStartEvent �RowNExposureEndEvent �RowNExposureStartEvent �EventsMissedEvent �BufferOverflowEvent</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string EventSelector = "EventSelector";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: Configures the exposure window in pixels.</para>
        /// <para>Availability: Zyla</para>
        /// </summary>
        public const string ExposedPixelHeight = "ExposedPixelHeight";


        /// <summary>
        /// <para>Setting Type: Float</para>
        /// <para>Description: The requested exposure time in seconds. Note: In some modes the exposure time can also be modified while the acquisition is running.</para>
        /// <para>Availability: Simcam, Neo, Zyla</para>
        /// </summary>
        public const string ExposureTime = "ExposureTime";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: When enabled this will notify the user on the Negative edge of the FIRE in Global Shutter and FIRE of Row 1 in Rolling Shutter.</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string ExposureEndEvent = "ExposureEndEvent";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: When enabled this will notify the user on the Positive edge of the FIRE in Global Shutter and FIRE of Row 1 in Rolling Shutter.</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string ExposureStartEvent = "ExposureStartEvent";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: When TRUE, the readout of the camera is no longer started by the external shutter. Instead, Pin 5 External Readout Start is used to start the readout.  The default value of this variable after initialization is FALSE.  Not available on Ascent.  Must use IOControl to cause the camera toconsider Pin 5 to be External Readout Start and not user-defined.</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string ExternalIOReadout = "ExternalIOReadout";


        /// <summary>
        /// <para>Setting Type: Float</para>
        /// <para>Description: Sets the delay time between the camera receiving an external trigger and the acquisition start.</para>
        /// <para>Availability: Zyla</para>
        /// </summary>
        public const string ExternalTriggerDelay = "ExternalTriggerDelay";


        /// <summary>
        /// <para>Setting Type: Enumerated</para>
        /// <para>Description: Configures the speed of the fan in the camera.  Options SimCam/Neo/Zyla: �Off �Low (Neo &SimCam Only) �On  Options Apogee: �Off �Low �Medium �High</para>
        /// <para>Availability: Simcam,Neo,Zyla,Apogee</para>
        /// </summary>
        public const string FanSpeed = "FanSpeed";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: Enables faster framerates at small AOIs.</para>
        /// <para>Availability: Zyla, Neo</para>
        /// </summary>
        public const string FastAOIFrameRateEnable = "FastAOIFrameRateEnable";


        /// <summary>
        /// <para>Setting Type: String</para>
        /// <para>Description: Returns the camera firmware version</para>
        /// <para>Availability: Neo,Zyla,Apogee</para>
        /// </summary>
        public const string FirmwareVersion = "FirmwareVersion";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: Choose whether to force the shutter to open. Overrides DisableShutter.</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string ForceShutterOpen = "ForceShutterOpen";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: Configures the number of images to acquire in the sequence. The value of FrameCount must be any value which is a multiple of AccumulateCount. This ensures the accumulation contains the correct number of frames. When this feature is unavailable then the camera does not currently support fixed length series, therefore you must explicitly abort the acquisition once you have acquired the amount of frames required.</para>
        /// <para>Availability: SimCam,Neo,Zyla,Apogee</para>
        /// </summary>
        public const string FrameCount = "FrameCount";


        /// <summary>
        /// <para>Setting Type: Float</para>
        /// <para>Description: The interval in seconds between the end of readout of one image to the beginning of exposure of the next.</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string FrameInterval = "FrameInterval";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: Configures whether the timing of image acquisition is determined by FrameInterval or FrameRate</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string FrameIntervalTiming = "FrameIntervalTiming";


        /// <summary>
        /// <para>Setting Type: Float</para>
        /// <para>Description: Configures the frame rate in Hz at which each image is acquired during any acquisition sequence. This is the rate at which frames are acquired by the camera which may be different from the rate at which frames are delivered to the user. For example when AccumulateCount has a value other than 1, the apparent frame rate will decrease proportionally.</para>
        /// <para>Availability: Simcam,Neo,Zyla,Apogee</para>
        /// </summary>
        public const string FrameRate = "FrameRate";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: Indicates whether or not the camera supports arbitrary AOI selection. If this feature is false then the supported AOI’s are limited to those listed in Section 4.6 Area of Interest.</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string FullAOIControl = "FullAOIControl";


        /// <summary>
        /// <para>Setting Type: Float</para>
        /// <para>Description: Returns the current Heatsink Temperature.  Not available for Ascent.</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string HeatSinkTemperature = "HeatSinkTemperature";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: Returns the buffer size in bytes required to store the data for one frame. This will be affected by the Area of Interest size, binning and whether metadata is appended to the data stream.</para>
        /// <para>Availability: Simcam, Neo, Zyla</para>
        /// </summary>
        public const string ImageSizeBytes = "ImageSizeBytes";


        /// <summary>
        /// <para>Setting Type: Float</para>
        /// <para>Description: Returns the operating input voltage to the camera.</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string InputVoltage = "InputVoltage";


        /// <summary>
        /// <para>Setting Type: String</para>
        /// <para>Description: Returns the camera interface type.  Current types:  Options Neo/Zyla: �USB3 �CL 3 Tap �CL 2x5 Tap �CL 10 Tap  Options Apogee: �USB2 �Ethernet</para>
        /// <para>Availability: Neo,Zyla,Apogee</para>
        /// </summary>
        public const string InterfaceType = "InterfaceType";


        /// <summary>
        /// <para>Setting Type: Enumerated</para>
        /// <para>Description: Configures whether selected IO is default or user defined.   Options  � Default  �User</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string IOControl = "IOControl";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: Configures whether selected IO is input or output. Cannot be changed for AltaF or Ascent.   Options �Input / 0 �Output / 1</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string IODirection = "IODirection";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: Configures whether selected IO is enabled or disabled.</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string IOState = "IOState";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: Indicates whether or not the operation of the IO Pin selected through the IO Selector Feature is inverted.</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string IOInvert = "IOInvert";


        /// <summary>
        /// <para>Setting Type: Enumerated</para>
        /// <para>Description: Selects the IO Pin that you subsequently wish to configure using the IO Invert Feature.  Options: �Fire 1 �Fire N (Zyla Only) �Aux Out 1 �Arm �External Trigger �Fire N and 1 (deprecated)  Options Apogee: �External Trigger (I/O Signal 1: TriggerNormal) �Fire (I/O Signal 2: Shutter Output) �Shutter Strobe (I/O Signal 3: Shutter Strobe) �External Exposure (I/O Signal 4 ExternalShutter) �External Readout (I/0 Signal 5: ExternalIOReadout)</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string IOSelector = "IOSelector";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: When TRUE, the camera normalizes the sensor before an image is taken with a flash of IR.  Not available for Ascent or other Interline transfer CCDs.</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string IRPreFlashEnable = "IRPreFlashEnable";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: Enableds/Disables any flushing command sent by the driver.</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string KeepCleanEnable = "KeepCleanEnable";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: Enables/Disables the camera control firmware to/from immediately beginning an internal flush cycle after an exposure.</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string KeepCleanPostExposureEnable = "KeepCleanPostExposure Enable";


        /// <summary>
        /// <para>Setting Type: Float</para>
        /// <para>Description: Configures the number of rows read per second.</para>
        /// <para>Availability: Zyla</para>
        /// </summary>
        public const string LineScanSpeed = "LineScanSpeed";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: Sets the position in the LUT to read/write a new pixel map</para>
        /// <para>Availability: any</para>
        /// </summary>
        public const string LUTIndex = "LUTIndex";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: Sets the value in LUT in position specified by LUT Index</para>
        /// <para>Availability: any</para>
        /// </summary>
        public const string LUTValue = "LUTValue";


        /// <summary>
        /// <para>Setting Type: Float</para>
        /// <para>Description: Returns the maximum sustainable transfer rate of the interface for the current shutter mode and AOI.</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string MaxInterfaceTransferRate = "MaxInterfaceTransferRate";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: Enable metadata. This is a global flag which will enable inclusion of metadata in the data stream as described in Section 4.5 Metadata. When this flag is enabled the data stream will always contain the MetadataFrame information. This will override the subsequent metadata settings when disabled. For example: If this feature is disabled and MetadataTimestamp is enabled, then metadata will not be included in the data stream. For example: If this feature is enabled and MetadataTimestamp is disabled, then metadata will be included in the data stream, but without timestamp information.</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string MetadataEnable = "MetadataEnable";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: Indicates whether the MetadataFrame information is included in the data stream.  This is read only and is automatically sent if metadata is enabled.</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string MetadataFrame = "MetadataFrame";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: Enables inclusion of timestamp information in the metadata stream. The timestamp indicates the time at which the exposure for the frame started.</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string MetadataTimestamp = "MetadataTimestamp";


        /// <summary>
        /// <para>Setting Type: String</para>
        /// <para>Description: Returns a revision code for the internal USB firmware within the camera head. Only available on USB interface.</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string MicrocodeVersion = "MicrocodeVersion";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: Configures whether the currently selected multitrack will be binned or not. Default state is set to true.</para>
        /// <para>Availability: Zyla</para>
        /// </summary>
        public const string MultitrackBinned = "MultitrackBinned";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: It is in the range 1-256. When set to 0, multitrack is disabled.</para>
        /// <para>Availability: Zyla</para>
        /// </summary>
        public const string MultitrackCount = "MultitrackCount";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: Configures the row at which the currently selected multitrack ends.</para>
        /// <para>Availability: Zyla</para>
        /// </summary>
        public const string MultitrackEnd = "MultitrackEnd";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: Selects multitrack index. It is in the range 0-255.</para>
        /// <para>Availability: Zyla</para>
        /// </summary>
        public const string MultitrackSelector = "MultitrackSelector";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: Configures the row at which the currently selected multitrack begins.</para>
        /// <para>Availability: Zyla</para>
        /// </summary>
        public const string MultitrackStart = "MultitrackStart";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: Enables overlap readout mode.</para>
        /// <para>Availability: Neo,Zyla,Apogee</para>
        /// </summary>
        public const string Overlap = "Overlap";


        /// <summary>
        /// <para>Setting Type: Enumerated</para>
        /// <para>Description: Configures the pixel correction to be applied.  Options: �Raw</para>
        /// <para>Availability: SimCam</para>
        /// </summary>
        public const string PixelCorrection = "PixelCorrection";


        /// <summary>
        /// <para>Setting Type: Enumerated</para>
        /// <para>Description: Configures the format of data stream.  See Section 4.4 Pixel Encoding.  Neo, Zyla and SimCam Options: Mono12 Mono12Packed Mono16 Mono32  SimCam only Options: Mono8 RGB8Packed Mono12Coded Mono12CodedPacked Mono22Parallel Mono22PackedParallel  See Section 4.7 PixelEncoding and PreAmpGainControl for the dependency between this feature and the PreAmpGainControl feature.</para>
        /// <para>Availability: Simcam, Neo, Zyla</para>
        /// </summary>
        public const string PixelEncoding = "PixelEncoding";


        /// <summary>
        /// <para>Setting Type: Float</para>
        /// <para>Description: Returns the height of each pixel in micrometers.</para>
        /// <para>Availability: SimCam,Neo,Zyla,Apogee</para>
        /// </summary>
        public const string PixelHeight = "PixelHeight";


        /// <summary>
        /// <para>Setting Type: Enumerated</para>
        /// <para>Description: Configures the rate of pixel readout from the sensor.  Options sCMOS: �280 MHz �200 MHz (Neo Only - deprecated) �100 MHz Options SimCam: �550 MHz Options Apogee: �Normal �Fast (Not Available on AltaE)</para>
        /// <para>Availability: Simcam,Neo,Zyla,Apogee</para>
        /// </summary>
        public const string PixelReadoutRate = "PixelReadoutRate";


        /// <summary>
        /// <para>Setting Type: Float</para>
        /// <para>Description: Returns the width of each pixel in micrometers.</para>
        /// <para>Availability: SimCam,Neo,Zyla,Apogee</para>
        /// </summary>
        public const string PixelWidth = "PixelWidth";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: Configures the port being used. Not implemented if fixed Single readout. Min is always 0. Max is {0, 1, 3} depending on {Single, Dual, Quad} readout.  If the camera is switchable between Single and Dual readout then the value of PortSelector required to set the gain/offset for Single readout is camera-dependent (i.e. it could be 0 or 1).</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string PortSelector = "PortSelector";


        /// <summary>
        /// <para>Setting Type: Enumerated</para>
        /// <para>Description: Configures the gain applied to the gain channel selected through the Pre Amp Gain Selector Feature.  Options: �x1 �x2 �x10 �x30</para>
        /// <para>Availability: Simcam,Neo(deprecated)</para>
        /// </summary>
        public const string PreAmpGain = "PreAmpGain";


        /// <summary>
        /// <para>Setting Type: Enumerated</para>
        /// <para>Description: Configures which pre amp gain channel(s) will be used for reading out the sensor.  Options: �High �Low �Both</para>
        /// <para>Availability: Simcam,Neo(deprecated)</para>
        /// </summary>
        public const string PreAmpGainChannel = "PreAmpGainChannel";


        /// <summary>
        /// <para>Setting Type: Enumerated</para>
        /// <para>Description: Wrapper Feature to simplify access to the PreAmpGain, PreAmpGainChannel and PreAmpGainSelector feaures. See Section 4.7 PixelEncoding and PreAmpGainControl for the dependency between this feature and the PixelEncoding feature. This feature is deprecated and should be replaced by the SimplePreAmpGainControl feature as some of the options may not be supported.  Options: �Gain 1 (11 bit) �Gain 2 (11 bit) �Gain 3 (11 bit) �Gain 4 (11 bit) �Gain 1 Gain 3 (16 bit) �Gain 1 Gain 4 (16 bit) �Gain 2 Gain 3 (16 bit) �Gain 2 Gain 4 (16 bit)</para>
        /// <para>Availability: Neo(deprecated)</para>
        /// </summary>
        public const string PreAmpGainControl = "PreAmpGainControl";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: This is the value of the pre-amplifier gain, for the currently- selected ADC/channel pair as selected by PortSelector if PortSelector is implemented.  Only effective for Fast PixelReadoutRate for AltaU/AltaE/AltaF/Aspen.</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string PreAmpGainValue = "PreAmpGainValue";


        /// <summary>
        /// <para>Setting Type: Enumerated</para>
        /// <para>Description: Selects the gain channel that you subsequently wish to configure using the Pre Amp Gain Feature.  Options: �High �Low</para>
        /// <para>Availability: Simcam,Neo(deprecated)</para>
        /// </summary>
        public const string PreAmpGainSelector = "PreAmpGainSelector";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: This is the value of the pre-amplifier offset, for the currently- selected ADC/channel pair as selected by PortSelector, if PortSelector is implemented.  Only effective for Fast PixelReadoutRate for AltaU/AltaE/AltaF/Aspen.</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string PreAmpOffsetValue = "PreAmpOffsetValue";


        /// <summary>
        /// <para>Setting Type: Float</para>
        /// <para>Description: This feature will return the time to readout data from a sensor.</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string ReadoutTime = "ReadoutTime";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: Enables Rolling Shutter Global Clear readout mode.</para>
        /// <para>Availability: Zyla</para>
        /// </summary>
        public const string RollingShutterGlobalClear = "RollingShutterGlobalClear";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: When enabled this will notify the user on the Negative edge of the FIRE of ROW N in Rolling Shutter.</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string RowNExposureEndEvent = "RowNExposureEndEvent";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: When enabled this will notify the user on the Positive edge of the FIRE of ROW N in Rolling Shutter.</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string RowNExposureStartEvent = "RowNExposureStartEvent";


        /// <summary>
        /// <para>Setting Type: Float</para>
        /// <para>Description: Configures the time in seconds to read a single row.</para>
        /// <para>Availability: Zyla</para>
        /// </summary>
        public const string RowReadTime = "RowReadTime";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: Configures whether the LineReadSpeed and RowReadTime can be altered.</para>
        /// <para>Availability: Zyla</para>
        /// </summary>
        public const string ScanSpeedControlEnable = "ScanSpeedControlEnable";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: Configures the state of the sensor cooling. Cooling is disabled by default at power up and must be enabled for the camera to achieve its target temperature. The actual target temperature can be set with the TemperatureControl feature where available for example on the Neo camera.</para>
        /// <para>Availability: Simcam, Neo, Zyla, Apogee</para>
        /// </summary>
        public const string SensorCooling = "SensorCooling";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: Returns the height of the sensor in pixels.</para>
        /// <para>Availability: Simcam,Neo,Zyla,Apogee</para>
        /// </summary>
        public const string SensorHeight = "SensorHeight";


        /// <summary>
        /// <para>Setting Type: String</para>
        /// <para>Description: Returns the sensor model installed in the camera.</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string SensorModel = "SensorModel";


        /// <summary>
        /// <para>Setting Type: Enumerated</para>
        /// <para>Description: Configures the direction in which the sensor will be read out.  Options �Bottom Up Sequential �Bottom Up Simultaneous �Centre Out Simultaneous �Outside In Simultaneous �Top Down Sequential �Top Down Simultaneous</para>
        /// <para>Availability: Zyla</para>
        /// </summary>
        public const string SensorReadoutMode = "SensorReadoutMode";


        /// <summary>
        /// <para>Setting Type: Enumerated</para>
        /// <para>Description: Returns true for CCD, False for CMOS.  Options: �CCD �CMOS</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string SensorType = "SensorType";


        /// <summary>
        /// <para>Setting Type: Float</para>
        /// <para>Description: Read the current temperature of the sensor.</para>
        /// <para>Availability: Simcam,Neo,Zyla,Apogee</para>
        /// </summary>
        public const string SensorTemperature = "SensorTemperature";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: Returns the width of the sensor in pixels.</para>
        /// <para>Availability: Simcam,Neo,Zyla,Apogee</para>
        /// </summary>
        public const string SensorWidth = "SensorWidth";


        /// <summary>
        /// <para>Setting Type: String</para>
        /// <para>Description: Returns the camera serial number.</para>
        /// <para>Availability: Simcam, Neo, Zyla</para>
        /// </summary>
        public const string SerialNumber = "SerialNumber";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: Disables the CCD voltage while the shutter strobe is high.</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string ShutterAmpControl = "ShutterAmpControl";


        /// <summary>
        /// <para>Setting Type: Enumerated</para>
        /// <para>Description: Controls the behavior of the shutter.  Options: �Open �Closed �Auto</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string ShutterMode = "ShutterMode";


        /// <summary>
        /// <para>Setting Type: Enumerated</para>
        /// <para>Description: Controls the mode the external trigger will run in. External Shutter signal can either be set to high (open) or low (closed). ShutterOutput can be triggered by setting AuxOutSourceTwo to ExternalShutterControl.  Options: �Open �Closed</para>
        /// <para>Availability: Zyla</para>
        /// </summary>
        public const string ShutterOutputMode = "ShutterOutputMode";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: Returns whether shutter is opened or closed.</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string ShutterState = "ShutterState";


        /// <summary>
        /// <para>Setting Type: Float</para>
        /// <para>Description: Sets the period of the shutter strobe on pin3.  Must use IOControl to cause the camera to consider Pin 3 to be Shutter Strobe Output and not user-defined.</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string ShutterStrobePeriod = "ShutterStrobePeriod";


        /// <summary>
        /// <para>Setting Type: Float</para>
        /// <para>Description: Sets the delay from the time the exposure begins to the time the rising edge of the shutter strobe period appears on pin 3.  Must use IOControl to cause the camera to consider Pin 3 to be Shutter Strobe Output and not user-defined.</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string ShutterStrobePosition = "ShutterStrobePosition";


        /// <summary>
        /// <para>Setting Type: Float</para>
        /// <para>Description: Sets the time at which the shutter will be opened before an exposure starts. Can be set between 0 and 100ms at 0.5ms steps.Only available whenever the ExternalTriggerMode is set to ‘Automatic’</para>
        /// <para>Availability: Zyla</para>
        /// </summary>
        public const string ShutterTransferTime = "ShutterTransferTime";


        /// <summary>
        /// <para>Setting Type: Enumerated</para>
        /// <para>Description: Wrapper Feature to simplify selection of the sensitivity and dynamic range options. This feature should be used as a replacement for the PreAmpGainControl feature as some of  the options in the PreAmpGainControl feature are not supported on all cameras. Supported Bit Depth will be dependent on the camera.  See Section 4.7 PixelEncoding  and PreAmpGainControl for the dependency between this feature and the PixelEncoding feature.  Options: �11-bit (high well capacity) Or 12-bit (high well capacity) �11-bit (low noise) Or 12-bit (low noise) �16-bit (low noise & high well capacity)</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string SimplePreAmpGainControl = "SimplePreAmpGainControl";


        /// <summary>
        /// <para>Setting Type: String</para>
        /// <para>Description: Returns the version of the SDK.</para>
        /// <para>Availability: System</para>
        /// </summary>
        public const string SoftwareVersion = "SoftwareVersion";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: Enables or Disables the Spurious Noise Filter</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string SpuriousNoiseFilter = "SpuriousNoiseFilter";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: Enables or Disables Static Blemish Correction</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string StaticBlemishCorrection = "StaticBlemishCorrection";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: Configures whether external triggers are synchronous with the read out of a sensor row. Asynchronous triggering may result in data corruption in the row being digitised when the triggers occurs.</para>
        /// <para>Availability: SimCam</para>
        /// </summary>
        public const string SynchronousTriggering = "Synchronous Triggering";


        /// <summary>
        /// <para>Setting Type: Float</para>
        /// <para>Description: Configures the temperature to which the sensor will be cooled. To be used for cameras with no correction data (-50- >25). Otherwise TemperatureControl should be used.</para>
        /// <para>Availability: Simcam,Neo(deprecated)</para>
        /// </summary>
        public const string TargetSensorTemperature = "TargetSensor Temperature";


        /// <summary>
        /// <para>Setting Type: Enumerated</para>
        /// <para>Description: Allows the user to set the target temperature of the sensor based on a list of valid temperatures.</para>
        /// <para>Availability: Neo</para>
        /// </summary>
        public const string TemperatureControl = "Temperature Control";


        /// <summary>
        /// <para>Setting Type: Enumerated</para>
        /// <para>Description: Reports the current state of cooling towards the Target Sensor Temperature. [Read Only]  Options Neo/Zyla/Apogee: �Cooler Off �Stabilised �Cooling  Neo/Zyla Only: �Drift �Not Stabilised �Fault  Apogee Only: �Backoff</para>
        /// <para>Availability: Neo,Zyla,Apogee</para>
        /// </summary>
        public const string TemperatureStatus = "Temperature Status";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: Reports the current value of the camera’s internal timestamp clock. This same clock is used to timestamp images as they are acquired when the MetadataTimestamp feature is enabled. The clock is reset to zero when the camera is powered on and then runs continuously at the frequency indicated by the TimestampClockFrequency feature. The clock is 64-bits wide.</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string TimestampClock = "TimestampClock";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: Reports the frequency of the camera’s internal timestamp clock in Hz.</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string TimestampClockFrequency = "TimestampClock Frequency";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: If false all image data will be concatenated and obtained as a single download at the end of the sequence. If true each image (or image row in the case of TDI) from a sequence will be available for download after it is read out (digitised).</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string TransmitFrames = "TransmitFrames";


        /// <summary>
        /// <para>Setting Type: Enumerated</para>
        /// <para>Description: Allows the user to configure the camera trigger mode at a high level. If the trigger mode is set to Advanced then the Trigger Selector and Trigger Source feature must also beset.  Neo, Zyla, SimCam and Apogee Options: �Internal �Software (Not available on Apogee) �External �External Start �External Exposure  SimCam only Options: �Advanced</para>
        /// <para>Availability: SimCam, Neo,Zyla, Apogee</para>
        /// </summary>
        public const string TriggerMode = "TriggerMode";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: Returns the USB Product ID associated with the camera system.  Only available for USB interface.</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string UsbProductId = "UsbProductId";


        /// <summary>
        /// <para>Setting Type: Integer</para>
        /// <para>Description: Returns the USB Device ID associated with the camera system.  Only available on USB interface.</para>
        /// <para>Availability: Apogee</para>
        /// </summary>
        public const string UsbDeviceId = "UsbDeviceId";


        /// <summary>
        /// <para>Setting Type: Boolean</para>
        /// <para>Description: Vertically centres the AOI in the frame. With this enabled, AOITop will be disabled.</para>
        /// <para>Availability: Neo, Zyla</para>
        /// </summary>
        public const string VerticallyCentreAOI = "VerticallyCentreAOI";


    }
}
