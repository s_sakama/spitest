﻿using System;

namespace AndorSDK
{
    //*** settings related methods
    public partial class AndorCamera : IDisposable {

        private void UpdateResultImageParameters() {
            this.simplePreAmpGainControl = GetSimplePreAmpGainControl();
            this.pixelEncoding = GetPixelEncoding();
            this.imageByteCount = GetImageByteCount();
            this.exposureTime = GetExposureTime();
            this.AOILeft = GetAOILeft();
            this.AOITop = GetAOITop();
            this.AOIWidth = GetAOIWidth();
            this.AOIHeight = GetAOIHeight();
            this.AOIStride = GetAOIStride();
            this.AOIBinning = GetAOIBinning();
        }

        //*** get

        private string GetSerial() {
            int getSerialSuccess = GetString(
                  SettingName.SerialNumber, out string serialString);
            if (getSerialSuccess == ATCore.AT_SUCCESS) {
                return serialString;
            }
            else {
                return "ERROR";
            }
        }

        private string GetModel() {
            int getModelSuccess = GetString(
                SettingName.CameraModel, out string model);
            if(getModelSuccess == ATCore.AT_SUCCESS) {
                return model;
            }
            else {
                return "ERROR";
            }
        }

        private PixelEncoding GetPixelEncoding() {
            int getEncodingSuccess = GetEnumerateString(
                  SettingName.PixelEncoding, out string encoding);
            if (getEncodingSuccess == ATCore.AT_SUCCESS) {
                return new PixelEncoding(encoding);
            }
            else {
                return new PixelEncoding("ERROR");
            }
        }

        private SimplePreAmpGainControl GetSimplePreAmpGainControl() {
            int getGainSuccess = GetEnumerateString(
                  SettingName.SimplePreAmpGainControl, out string mode);
            if (getGainSuccess == ATCore.AT_SUCCESS) {
                return new SimplePreAmpGainControl(mode);
            }
            else {
                return new SimplePreAmpGainControl("ERROR");
            }
        }

        private int GetImageByteCount() {
            int getIBCSuccess = GetInt(
                  SettingName.ImageSizeBytes, out long imageByteCount);
            if (getIBCSuccess == ATCore.AT_SUCCESS) {
                return (int) imageByteCount; //byte count normally not overflow int
            }
            else {
                return -1;
            }
        }

        private int GetAOITop() {
            int getAOITopSuccess = GetInt(
                  SettingName.AOITop, out long AOITop);
            if (getAOITopSuccess == ATCore.AT_SUCCESS) {
                return (int) AOITop; //AOITop normally not overflow int
            }
            else {
                return -1;
            }
        }

        private int GetAOILeft() {
            int getAOILeftSuccess = GetInt(
                  SettingName.AOILeft, out long AOILeft);
            if (getAOILeftSuccess == ATCore.AT_SUCCESS) {
                return (int) AOILeft; //AOILeft normally not overflow int
            }
            else {
                return -1;
            }
        }

        private int GetAOIWidth() {
            int getAOIWidthSuccess = GetInt(
                  SettingName.AOIWidth, out long AOIWidth);
            if (getAOIWidthSuccess == ATCore.AT_SUCCESS) {
                return (int) AOIWidth; //AOIWidth normally not overflow int
            }
            else {
                return -1;
            }
        }

        private int GetAOIHeight() {
            int getAOIHeightSuccess = GetInt(
                  SettingName.AOIHeight, out long AOIHeight);
            if (getAOIHeightSuccess == ATCore.AT_SUCCESS) {
                return (int) AOIHeight; //AOIHeight normally not overflow int
            }
            else {
                return -1;
            }
        }

        private int GetAOIStride() {
            int getAOIStrideSuccess = GetInt(
                  SettingName.AOIStride, out long AOIStride);
            if (getAOIStrideSuccess == ATCore.AT_SUCCESS) {
                return (int) AOIStride; //AOIStride normally not overflow int
            }
            else {
                return -1;
            }
        }

        private AOIBinningMode GetAOIBinning() {
            int getEncodingSuccess = GetEnumerateString(
                  SettingName.AOIBinning, out string binning);
            if (getEncodingSuccess == ATCore.AT_SUCCESS) {
                return new AOIBinningMode(binning);
            }
            else {
                return new AOIBinningMode("ERROR");
            }
        }

        private int GetMaxAOIWidth()
        {
            long maxAOIWidth = 0;

            int getMaxAOIWidthSuccess = ATCore.AT_GetIntMax(
                  cameraHandle, SettingName.AOIWidth, ref maxAOIWidth);
            if(getMaxAOIWidthSuccess == ATCore.AT_SUCCESS)
            {
                return (int) maxAOIWidth;
            } else
            {
                return -1;
            }
        }

        private int GetMaxAOIHeight()
        {
            long maxAOIHeight = 0;

            int getMaxAOIWidthSuccess = ATCore.AT_GetIntMax(
                  cameraHandle, SettingName.AOIWidth, ref maxAOIHeight);
            if (getMaxAOIWidthSuccess == ATCore.AT_SUCCESS)
            {
                return (int)maxAOIHeight;
            }
            else
            {
                return -1;
            }
        }

        private double GetExposureTime() {
            int getExposureTimeSuccess = GetFloat(
                   SettingName.ExposureTime, out double exposureTime);
            if(getExposureTimeSuccess == ATCore.AT_SUCCESS) {
                return exposureTime;
            }
            else {
                return -1.0;
            }
        }

        private double GetFrameRate() {
            int getFrameRateSuccess = GetFloat(
                  SettingName.FrameRate, out double frameRate);
            if(getFrameRateSuccess == ATCore.AT_SUCCESS) {
                return frameRate;
            }
            else {
                return -1.0;
            }
        }

        public double GetMaxFrameRate() {
            //max camera capability at this settings
            double cameraMaxFrameRate = 0.0;
            int getMaxCameraFrameRateSuccess = ATCore.AT_GetFloatMax(
                  cameraHandle, SettingName.FrameRate, ref cameraMaxFrameRate);

            //max connection capability at this settings
            double connectionMaxFrameRate = 0.0;
            int getMaxConnectionFrameRateSuccess = ATCore.AT_GetFloatMax(
                  cameraHandle, SettingName.MaxInterfaceTransferRate,
                  ref connectionMaxFrameRate);
            connectionMaxFrameRate *= 0.9; //for sustainability
            
            if((getMaxCameraFrameRateSuccess == ATCore.AT_SUCCESS) &&
               (getMaxConnectionFrameRateSuccess == ATCore.AT_SUCCESS)) {

                return Math.Min(cameraMaxFrameRate, connectionMaxFrameRate);
            }
            else if (getMaxCameraFrameRateSuccess == ATCore.AT_SUCCESS) {
                return cameraMaxFrameRate;
            }
            else if (getMaxConnectionFrameRateSuccess == ATCore.AT_SUCCESS) {
                return connectionMaxFrameRate;
            }
            else {
                return -1.0;
            }
        }

        private bool? GetRollingShutterGlobalClear()
        {
            int getRollingShutterGlobalClearSuccess = GetBool(SettingName.RollingShutterGlobalClear, out bool rollingShutterGlobalClear);
            if(getRollingShutterGlobalClearSuccess == ATCore.AT_SUCCESS)
            {
                return rollingShutterGlobalClear;
            } else
            {
                return false;
            }
        }

        private ElectronicShutteringMode GetElectronicShutteringMode()
        {
            int getElectronicShutteringMode = GetEnumerateString(SettingName.ElectronicShutteringMode, out string mode);
            if(getElectronicShutteringMode == ATCore.AT_SUCCESS)
            {
                return new ElectronicShutteringMode(mode);
            } else
            {
                return new ElectronicShutteringMode("ERROR");
            }
        }

        private PixelReadoutRate GetPixelReadoutRate()
        {
            int PixelReadoutRate = GetEnumerateString(SettingName.ElectronicShutteringMode, out string mode);
            if (PixelReadoutRate == ATCore.AT_SUCCESS)
            {
                return new PixelReadoutRate(mode);
            }
            else
            {
                return new PixelReadoutRate("ERROR");
            }
        }

        //*** set

        /// <returns>Succes Code, check constant int AndorSDK.cs</returns>
        public int SetPixelEncoding(PixelEncoding pixelEncoding) {
            int successCode = SetEnumeratedString(
                    SettingName.PixelEncoding, pixelEncoding.encoding);
            //set success or not, try update local field
            this.pixelEncoding = GetPixelEncoding();

            return successCode;
        }

        public int SetSimplePreAmpGainControl(SimplePreAmpGainControl simplePreAmpGainControl) {
            int successCode = SetEnumeratedString(
                  SettingName.SimplePreAmpGainControl, simplePreAmpGainControl.mode);
            //set success or not, try update local field
            this.simplePreAmpGainControl = GetSimplePreAmpGainControl();
            this.pixelEncoding = GetPixelEncoding();

            return successCode;
        }

        /// <returns>Succes Code, check constant int AndorSDK.cs</returns>
        public int SetExposureTime(double exposureTime) {
            int successCode = SetFloat(
                  SettingName.ExposureTime, exposureTime);

            //if (successCode == ATCore.AT_SUCCESS) {
            //    //set framerate to maximum
            //    double maxFrameRate = 0.0;
            //    ATCore.AT_GetFloatMax(
            //      cameraHandle, SettingName.FrameRate, ref maxFrameRate);
            //    SetFloat(SettingName.FrameRate, maxFrameRate);
            //}

            //set success or not, try update local field
            this.exposureTime = GetExposureTime();
            //this.frameRate = GetFrameRate();

            return successCode;
        }

        /// <returns>Succes Code, check constant int AndorSDK.cs</returns>
        public int SetFrameRate(double frameRate) {
            double maxFrameRate = GetMaxFrameRate();
            double targetFrameRate = Math.Min(maxFrameRate, frameRate);

            int successCode = SetFloat(
                  SettingName.FrameRate, targetFrameRate);

            //set success or not, try update local field
            this.frameRate = GetFrameRate();

            return successCode;
        }

        /// <returns>Succes Code, check constant int AndorSDK.cs</returns>
        public int SetFrameRateToMaximum() {
            double maxFrameRate = GetMaxFrameRate();
            int successCode;
            if(maxFrameRate > 0.0) {
                successCode = SetFloat(
                  SettingName.FrameRate, maxFrameRate);
            }
            else {
                successCode = ATCore.AT_ERR_NOTWRITABLE;
            }

            //set success or not, try update local field
            this.frameRate = GetFrameRate();

            return successCode;
        }

        /// <returns>Succes Code, check constant int AndorSDK.cs</returns>
        public int SetTriggerMode(TriggerMode triggerMode) {
        return SetEnumeratedString(
                     SettingName.TriggerMode, triggerMode.mode);
        }

        /// <returns>Succes Code, check constant int AndorSDK.cs</returns>
        public int SetCycleMode(CycleMode cycleMode) {
            return SetEnumeratedString(
                     SettingName.CycleMode, cycleMode.mode);
        }

        /// <returns>Succes Code, check constant int AndorSDK.cs</returns>
        public int SetSensorCooling(bool enable) {
            return SetBool(
                     SettingName.SensorCooling, enable);
        }
        
        /// <returns>Succes Code, check constant int AndorSDK.cs</returns>
        public int SetAOIBinning(AOIBinningMode AOIBinning) {
            int successCode = SetEnumeratedString(
                    SettingName.AOIBinning, AOIBinning.mode);
            
            //set success or not, try update local field
            this.AOIBinning = GetAOIBinning();

            return successCode;
        }

        /// <returns>Succes Code, check constant int AndorSDK.cs</returns>
        public int SetAOITop(int AOITop) {
            //try set
            int successCode = SetInt(
                    SettingName.AOITop, AOITop);

            //success or not, update local field
            this.AOITop = GetAOITop();

            return successCode;
        }

        /// <returns>Succes Code, check constant int AndorSDK.cs</returns>
        public int SetAOILeft(int AOILeft) {
            int successCode = SetInt(
                    SettingName.AOILeft, AOILeft);

            //set success or not, try update local field
            this.AOILeft = GetAOILeft();

            return successCode;
        }

        /// <returns>Succes Code, check constant int AndorSDK.cs</returns>
        public int SetAOIWidth(int AOIWidth) {
            int successCode = SetInt(
                  SettingName.AOIWidth, AOIWidth);

            //set success or not, try update local field
            this.AOIWidth = GetAOIWidth();

            return successCode;
        }

        /// <returns>Succes Code, check constant int AndorSDK.cs</returns>
        public int SetAOIHeight(int AOIHeight) {
            int successCode = SetInt(
                  SettingName.AOIHeight, AOIHeight);

            //set success or not, try update local field
            this.AOIHeight = GetAOIHeight();

            return successCode;
        }

        public void SetMaxAOIHeight()
        {
            this.MaxAOIHeight = GetMaxAOIHeight();
        }

        public void SetMaxAOIWidth()
        {
            this.MaxAOIWidth = GetMaxAOIWidth();
        }

        /// <returns>Succes Code, check constant int AndorSDK.cs</returns>
        public int SetRollingShutterGlobalClear(bool value)
        {
            int successCode = SetBool(SettingName.RollingShutterGlobalClear, value);
            bool? v = GetRollingShutterGlobalClear();
            if (v.HasValue)
            {
                RollingShutterGlobalClear = v.Value;
            }
            return successCode;
        }

        public int SetElectronicShutteringMode(ElectronicShutteringMode electronicShutteringMode)
        {
            int successCode = SetEnumeratedString(SettingName.ElectronicShutteringMode, electronicShutteringMode.mode);
            this.ElectronicShutteringMode = GetElectronicShutteringMode();
            return successCode;
        }

        public int SetPixelReadoutRate(PixelReadoutRate pixelReadoutRate)
        {
            int successCode = SetEnumeratedString(SettingName.PixelReadoutRate, pixelReadoutRate.mode);
            this.PixelReadout = GetPixelReadoutRate();
            return successCode;
        }
    }
}

