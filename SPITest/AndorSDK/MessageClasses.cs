﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AndorSDK {
    //*** message classes

    public class CameraInfo {
        public int cameraHandle { get; set; }
        public CameraInfo(int cameraHandle) {
            this.cameraHandle = cameraHandle;
        }
    }

    public class PixelEncoding : IEquatable<PixelEncoding> { //enum-like
        public string encoding { get; set; }
        public PixelEncoding(string encoding) {
            this.encoding = encoding;
        }
        public static PixelEncoding Mono12 {
            get { return new PixelEncoding("Mono12"); }
        }
        public static PixelEncoding Mono12Packed {
            get { return new PixelEncoding("Mono12Packed"); }
        }
        public static PixelEncoding Mono16 {
            get { return new PixelEncoding("Mono16"); }
        }
        public static PixelEncoding Mono32 {
            get { return new PixelEncoding("Mono32"); }
        }
        public static PixelEncoding ERROR {
            get { return new PixelEncoding("ERROR"); }
        }

        //comparer
        public static bool operator ==(PixelEncoding p1, PixelEncoding p2) {
            return p1.encoding == p2.encoding;
        }
        public static bool operator !=(PixelEncoding p1, PixelEncoding p2) {
            return p1.encoding != p2.encoding;
        }
        public bool Equals(PixelEncoding other) {
            return this.encoding == other.encoding;
        }
        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) { return false; }
            return obj.GetType() == GetType() && Equals((PixelEncoding)obj);
        }
        public override int GetHashCode() {
            return encoding.GetHashCode();
        }
    }

    public class TriggerMode : IEquatable<TriggerMode> { //enum-like
        public string mode { get; set; }
        public TriggerMode(string mode) {
            this.mode = mode;
        }
        public static TriggerMode SoftWare {
            get { return new TriggerMode("SoftWare"); }
        }
        public static TriggerMode Internal {
            get { return new TriggerMode("Internal"); }
        }
        public static TriggerMode External {
            get { return new TriggerMode("External"); }
        }
        public static TriggerMode ExternalStart {
            get { return new TriggerMode("External Start"); }
        }
        public static TriggerMode ExternalExposure {
            get { return new TriggerMode("External Exposure"); }
        }

        //comparer
        public static bool operator ==(TriggerMode t1, TriggerMode t2) {
            return t1.mode == t2.mode;
        }
        public static bool operator !=(TriggerMode t1, TriggerMode t2) {
            return t1.mode != t2.mode;
        }
        public bool Equals(TriggerMode other) {
            return this.mode == other.mode;
        }
        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) { return false; }
            return obj.GetType() == GetType() && Equals((TriggerMode)obj);
        }
        public override int GetHashCode() {
            return mode.GetHashCode();
        }
    }

    public class CycleMode : IEquatable<CycleMode> { //enum-like
        public string mode { get; set; }
        public CycleMode(string mode) {
            this.mode = mode;
        }
        public static CycleMode Continuous {
            get { return new CycleMode("Continuous"); }
        }
        public static CycleMode Fixed {
            get { return new CycleMode("Fixed"); }
        }

        //comparer
        public static bool operator ==(CycleMode t1, CycleMode t2) {
            return t1.mode == t2.mode;
        }
        public static bool operator !=(CycleMode t1, CycleMode t2) {
            return t1.mode != t2.mode;
        }
        public bool Equals(CycleMode other) {
            return this.mode == other.mode;
        }
        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) { return false; }
            return obj.GetType() == GetType() && Equals((CycleMode)obj);
        }
        public override int GetHashCode() {
            return mode.GetHashCode();
        }
    }

    public class AOIBinningMode : IEquatable<AOIBinningMode> { //enum-like
        public string mode { get; set; }
        public AOIBinningMode(string binning) {
            this.mode = binning;
        }
        public static AOIBinningMode bin_1x1 {
            get { return new AOIBinningMode("1x1"); }
        }
        public static AOIBinningMode bin_2x2 {
            get { return new AOIBinningMode("2x2"); }
        }
        public static AOIBinningMode bin_3x3 {
            get { return new AOIBinningMode("3x3"); }
        }
        public static AOIBinningMode bin_4x4 {
            get { return new AOIBinningMode("4x4"); }
        }
        public static AOIBinningMode bin_8x8 {
            get { return new AOIBinningMode("8x8"); }
        }

        //compare
        public static bool operator ==(AOIBinningMode t1, AOIBinningMode t2) {
            return t1.mode == t2.mode;
        }
        public static bool operator !=(AOIBinningMode t1, AOIBinningMode t2) {
            return t1.mode != t2.mode;
        }
        public bool Equals(AOIBinningMode other) {
            return this.mode == other.mode;
        }
        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) { return false; }
            return obj.GetType() == GetType() && Equals((AOIBinningMode)obj);
        }
        public override int GetHashCode() {
            return mode.GetHashCode();
        }
    }

    public class SimplePreAmpGainControl :
                   IEquatable<SimplePreAmpGainControl> { //enum-like
        public string mode { get; set; }
        public SimplePreAmpGainControl(string mode) {
            this.mode = mode;
        }
        public static SimplePreAmpGainControl high_capacity_12bit {
            get { return new SimplePreAmpGainControl(
                               "12-bit (high well capacity)"); }
        }
        public static SimplePreAmpGainControl low_noise_12bit {
            get {
                return new SimplePreAmpGainControl(
                             "12-bit (low noise)");
            }
        }
        public static SimplePreAmpGainControl high_capacity_16bit {
            get {
                return new SimplePreAmpGainControl(
                             "16-bit (low noise & high well capacity)");
            }
        }

        //compare
        public static bool operator ==(SimplePreAmpGainControl t1, SimplePreAmpGainControl t2) {
            return t1.mode == t2.mode;
        }
        public static bool operator !=(SimplePreAmpGainControl t1, SimplePreAmpGainControl t2) {
            return t1.mode != t2.mode;
        }
        public bool Equals(SimplePreAmpGainControl other) {
            return this.mode == other.mode;
        }
        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) { return false; }
            return obj.GetType() == GetType() && Equals((SimplePreAmpGainControl)obj);
        }
        public override int GetHashCode() {
            return mode.GetHashCode();
        }
    }

    public class ElectronicShutteringMode : IEquatable<ElectronicShutteringMode>
    {
        public string mode { get; set; }
        public ElectronicShutteringMode(string mode)
        {
            this.mode = mode;
        }

        public static ElectronicShutteringMode rolling
        {
            get
            {
                return new ElectronicShutteringMode("Rolling");
            }
        }

        public static ElectronicShutteringMode global
        {
            get
            {
                return new ElectronicShutteringMode("Global");
            }
        }


        //compare
        public static bool operator ==(ElectronicShutteringMode t1, ElectronicShutteringMode t2)
        {
            return t1.mode == t2.mode;
        }
        public static bool operator !=(ElectronicShutteringMode t1, ElectronicShutteringMode t2)
        {
            return t1.mode != t2.mode;
        }
        public bool Equals(ElectronicShutteringMode other)
        {
            return this.mode == other.mode;
        }
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) { return false; }
            return obj.GetType() == GetType() && Equals((ElectronicShutteringMode)obj);
        }
        public override int GetHashCode()
        {
            return mode.GetHashCode();
        }
    }

    /// <summary>
    /// For simple coding.
    /// </summary>
    public class AndorEnumType : IEquatable<AndorEnumType>
    {
        public string mode { get; set; }

        public AndorEnumType(string mode)
        {
            this.mode = mode;
        }

        //compare
        public static bool operator ==(AndorEnumType t1, AndorEnumType t2)
        {
            return t1.mode == t2.mode;
        }
        public static bool operator !=(AndorEnumType t1, AndorEnumType t2)
        {
            return t1.mode != t2.mode;
        }
        public bool Equals(AndorEnumType other)
        {
            return this.mode == other.mode;
        }
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) { return false; }
            return obj.GetType() == GetType() && Equals((AndorEnumType)obj);
        }
        public override int GetHashCode()
        {
            return mode.GetHashCode();
        }
    }

    public class PixelReadoutRate : AndorEnumType
    {
        public PixelReadoutRate(string mode) : base(mode) { }

        public static PixelReadoutRate rate_550_simcamOnly
        {
            get
            {
                return new PixelReadoutRate("550 MHz");
            }
        }

        public static PixelReadoutRate rate_280
        {
            get
            {
                return new PixelReadoutRate("280 MHz");
            }
        }

        public static PixelReadoutRate rate_100
        {
            get
            {
                return new PixelReadoutRate("100 MHz");
            }
        }
    }
}
