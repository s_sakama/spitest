﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AndorSDK {
    public class ManagedBuffers {
        private int numberOfBuffers;
        private int queueIndex = 0;
        private int pixelCount;
        private ushort[][] ListBuffers;

        public ManagedBuffers(int numberOfBuffers, int pixelCount) {
            this.numberOfBuffers = numberOfBuffers;
            this.pixelCount = pixelCount;

            Allocate();
        }

        private void Allocate() {
            ListBuffers = new ushort[numberOfBuffers][];
            for (int i = 0; i < ListBuffers.Length; i++) {
                ListBuffers[i] = new ushort[pixelCount];
            }
        }

        public void QueueNextBuffer() {
            queueIndex += 1;
        }

        public ushort[] GetCurrentBuffer() {
            int targetIndex = queueIndex % numberOfBuffers;
            return ListBuffers[targetIndex];
        }
    }
}
