﻿using System;
using System.Collections.Generic;

namespace AndorSDK
{
    //*** base, constructor, diposer, singleton patterns
    public partial class AndorCamera : IDisposable {

        //*** properties unique to each camera

        public int cameraHandle { get; private set; }
        public int cameraIndex { get; }
        public bool IsDisposed { get; private set; }
        public bool IsAcquiring { get; private set; }
        public string serial { get; }
        public string cameraModel { get; }
        public PixelEncoding pixelEncoding { get; private set; }
        public SimplePreAmpGainControl simplePreAmpGainControl { get; private set; }
        public double exposureTime { get; private set; }
        public double frameRate { get; private set; }
        public int imageByteCount { get; private set; }
        public int AOILeft { get; private set; }
        public int AOITop { get; private set; }
        public int AOIWidth { get; private set; }
        public int AOIHeight { get; private set; }
        public int MaxAOIWidth { get; private set; }
        public int MaxAOIHeight { get; private set; }
        public int AOIStride { get; private set; }
        public AOIBinningMode AOIBinning { get; set; }
        public bool RollingShutterGlobalClear { get; private set; }
        public ElectronicShutteringMode ElectronicShutteringMode { get; private set; }
        public PixelReadoutRate PixelReadout { get; private set; }

        //*** constructor

        /// <summary>
        /// Connect to a camera, initialized library if needed, open camera if needed.
        /// If the target camra (denoted by cameraIndex) is already open and used by
        /// other AndorCamera object, it is fine too. You just get another object
        /// point to the same camera. 
        /// </summary>
        public AndorCamera(int cameraIndex) { //constructor
            this.cameraHandle = OpenCamera(cameraIndex);
            this.cameraIndex = cameraIndex;
            this.IsDisposed = false;
            this.IsAcquiring = false;
            this.serial = GetSerial();
            this.cameraModel = GetModel();
            UpdateResultImageParameters();
            SetSensorCooling(true);
        }

        //*** disposer

        public void Dispose() { //follow disposing pattern , this is important because this class have unmanaged memory
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool isDisposing) {//follow disposing pattern, just care between //my code start and //my code end
            try {
                if (!this.IsDisposed) {
                    if (isDisposing) {
                        //my code start
                        //many camera object might point to single physical camera
                        //(allows multiple class to control camera)
                        //thus, decrease object count, only disconnect when no object
                        CloseCamera(cameraIndex, cameraHandle);
                        //my code end
                    }
                }
            }
            finally {
                this.IsDisposed = true;
            }
        }

        ~AndorCamera() { //finalizer, follow disposing pattern
            Dispose(false);
        }

        private void RestartConnection() {
            ATCore.AT_Close(cameraHandle);
            int tempCameraHandle = 0;
            ATCore.AT_Open(cameraIndex, ref tempCameraHandle);
            cameraHandle = tempCameraHandle;
        }

        //*** static properties, shared to all camera

        private static bool libraryInitialized { get; set; } = false;
        /// <summary> handle of opened cameras &lt;cameraIndex,handle&rt;" </summary>
        private static Dictionary<int, CameraInfo> openedCamerasInfo { get; set; } = new Dictionary<int, CameraInfo>();

        //*** static helper method

        private static void InitializeLibrary() {
            int initializeResult = ATCore.AT_InitialiseLibrary();
            int initializeUtilResult = ATCore.AT_InitialiseUtilityLibrary();
            if ((initializeResult == ATCore.AT_SUCCESS)
                && (initializeUtilResult == ATCore.AT_SUCCESS)) {
                libraryInitialized = true;
            }
            else {
                string message = $"Cannot do AT_InitialiseLibrary. Return value {initializeResult.ToString()}";
                throw new InvalidOperationException(message);
            }
        }

        /// <summary> try open camera, return cameraHandle </summary>
        private static int OpenCamera(int targetCameraIndex) {
            if (!libraryInitialized) {
                InitializeLibrary();
            }


            if (openedCamerasInfo.ContainsKey(targetCameraIndex)) {
                //this camera is already open
                string message = $"Camera with index {targetCameraIndex.ToString()} (handle {openedCamerasInfo[targetCameraIndex].cameraHandle.ToString()}) is already open";
                throw new InvalidOperationException(message);
            }
            else {
                //camera is not opened, try open it
                int cameraHandle = 0;
                int openCameraResult = ATCore.AT_Open(targetCameraIndex, ref cameraHandle);
                if (openCameraResult == ATCore.AT_SUCCESS) {

                    openedCamerasInfo.Add(targetCameraIndex,
                        new CameraInfo(cameraHandle));
                    return cameraHandle;
                }
                else {
                    string message = $"Cannot open camera {targetCameraIndex.ToString()}. Return value {openCameraResult.ToString()}";
                    throw new InvalidOperationException(message);
                }
            }
        }

        private static void CloseCamera(int cameraIndex, int cameraHandle) {
            ATCore.AT_Close(cameraHandle);
            if (openedCamerasInfo.ContainsKey(cameraIndex)) {
                openedCamerasInfo.Remove(cameraIndex);
                if (openedCamerasInfo.Count <= 0) {
                    if (libraryInitialized) {
                        ATCore.AT_FinaliseLibrary();
                        ATCore.AT_FinaliseUtilityLibrary();
                        libraryInitialized = false;
                    }
                }
            }
        }
    }
}
