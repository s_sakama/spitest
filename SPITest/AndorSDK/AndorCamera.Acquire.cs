﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Threading;
using BackgroundWorker = System.ComponentModel.BackgroundWorker;
using DoWorkEventArgs = System.ComponentModel.DoWorkEventArgs;
using ProgressChangedEventArgs = System.ComponentModel.ProgressChangedEventArgs;
using RunWorkerCompletedEventArgs = System.ComponentModel.RunWorkerCompletedEventArgs;

namespace AndorSDK
{
    //*** acquiring related methods
    public partial class AndorCamera : IDisposable {

        public List<ushort[]> AcquireMulti(int frameCount) {
            var dataList = new List<ushort[]>();

            this.IsAcquiring = true;

            SetTriggerMode(TriggerMode.SoftWare);
            SetCycleMode(CycleMode.Continuous);

            UpdateResultImageParameters();

            //queue buffers
            int numberOfBuffers = 30;
            var unmanagedAlignedBuffer = new UnmanagedAlignedBuffers(numberOfBuffers, cameraHandle, imageByteCount);
            managedBuffers = new ManagedBuffers(numberOfBuffers, AOIWidth * AOIHeight);

            //acquire
            ATCore.AT_Command(cameraHandle, "Acquisition Start");
            for (int frameIndex = 0; frameIndex < frameCount; frameIndex++) {
                dataList.Add(CaptureFrame(timeout: 10000));
                unmanagedAlignedBuffer.QueueNextBuffer();
                managedBuffers.QueueNextBuffer();
            }
            ATCore.AT_Command(cameraHandle, "AcquisitionStop");
            ATCore.AT_Flush(cameraHandle);
            this.IsAcquiring = false;

            //release buffer
            unmanagedAlignedBuffer.Release();

            return dataList;
        }

        private ManagedBuffers managedBuffers;
        private ushort[] CaptureFrame(uint timeout) {
            var captureFrameResult = managedBuffers.GetCurrentBuffer();

            //recieve frame
            int currentImageByteCount = imageByteCount;
            IntPtr readBuffer = new IntPtr();
            int acquisitionSuccess = ATCore.AT_WaitBuffer(cameraHandle, ref readBuffer, ref currentImageByteCount, timeout);

            //convert
            if (acquisitionSuccess == ATCore.AT_SUCCESS) {
                //Marshal.AllocHGlobal doesn't support ushort so use pointer instead
                unsafe {
                    fixed (ushort* ptrResult = &captureFrameResult[0]) {
                        ATCore.AT_ConvertBuffer(readBuffer, (IntPtr)ptrResult
                                        , AOIWidth, AOIHeight, AOIStride
                                        , pixelEncoding.encoding
                                        , PixelEncoding.Mono16.encoding);
                    }
                }
            }
            return captureFrameResult;
        }

        //** continuous acquire using backgroundworker (for .NET 4.5, consider task.Run() )
        
        private BackgroundWorker bgwContinuousAcquire;
        public bool continuousAcquireBusyRendering { get; set; } = false;

        public void AcquireContinuousStart() {
            this.IsAcquiring = true;
            bgwContinuousAcquire = new BackgroundWorker();
            bgwContinuousAcquire.DoWork -= bgwContinuousAcquire_DoWork;
            bgwContinuousAcquire.DoWork += bgwContinuousAcquire_DoWork;
            bgwContinuousAcquire.ProgressChanged -= bgwContinuousAcquire_ProgressChanged;
            bgwContinuousAcquire.ProgressChanged += bgwContinuousAcquire_ProgressChanged;
            bgwContinuousAcquire.RunWorkerCompleted -= bgwContinuousAcquire_RunWorkerCompleted;
            bgwContinuousAcquire.RunWorkerCompleted += bgwContinuousAcquire_RunWorkerCompleted;
            bgwContinuousAcquire.WorkerReportsProgress = true;
            bgwContinuousAcquire.WorkerSupportsCancellation = true;
            bgwContinuousAcquire.RunWorkerAsync();
        }

        [Obsolete]
        public void AcquireContinuousStop() {
            if (bgwContinuousAcquire == null) { return; }
            bgwContinuousAcquire.CancelAsync();
            while (bgwContinuousAcquire.IsBusy) {
                DoEvents();
                /*Application.DoEvents();*/ } //wait backgroundworker to finish cancel
            bgwContinuousAcquire.DoWork -= bgwContinuousAcquire_DoWork;
            bgwContinuousAcquire.ProgressChanged -= bgwContinuousAcquire_ProgressChanged;
            bgwContinuousAcquire.RunWorkerCompleted -= bgwContinuousAcquire_RunWorkerCompleted;
            this.IsAcquiring = false;
        }

        // In WPF, Application.DoEvents() is not available.
        public void DoEvents()
        {
            DispatcherFrame frame = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
                new DispatcherOperationCallback(ExitFrames), frame);
            Dispatcher.PushFrame(frame);
        }

        public object ExitFrames(object f)
        {
            ((DispatcherFrame)f).Continue = false;

            return null;
        }

        private void bgwContinuousAcquire_DoWork(Object sender, DoWorkEventArgs e) {
            //settings
            SetTriggerMode(TriggerMode.SoftWare);
            SetCycleMode(CycleMode.Continuous);
            UpdateResultImageParameters();
            //MessageBox.Show(pixelEncoding.encoding + " " + imageByteCount.ToString() + " " + AOILeft.ToString() + " " + AOITop.ToString() + " " + AOIWidth.ToString() + " " + AOIHeight.ToString() + " " + AOIStride.ToString() + " " + AOIBinning.mode + " " + frameRate.ToString());

            //queue buffers
            int numberOfBuffers = 30;
            var unmanagedAlignedBuffer = new UnmanagedAlignedBuffers(numberOfBuffers, cameraHandle, imageByteCount);
            managedBuffers = new ManagedBuffers(numberOfBuffers, AOIWidth * AOIHeight);

            //acquire
            int frameIndex = 0;
            ATCore.AT_Command(cameraHandle, "Acquisition Start");
            while (true) {
                if (bgwContinuousAcquire.CancellationPending) {
                    e.Cancel = true;
                    break;
                }

                frameIndex++;
                var ev = new NewFrameArrivedEventArgs(frameIndex
                                                     , CaptureFrame(timeout: 100000));

                //if (!continuousAcquireBusyRendering) { //owner ready to recieve frame
                    //continuousAcquireBusyRendering = true;
                    bgwContinuousAcquire.ReportProgress(0, ev);
                //}

                unmanagedAlignedBuffer.QueueNextBuffer();
                managedBuffers.QueueNextBuffer();
            }

            //MessageBox.Show("test");

            int stopSucess = ATCore.AT_Command(cameraHandle, "AcquisitionStop");
            ATCore.AT_Flush(cameraHandle);
            RestartConnection(); //prevent simcam cpu eating bug

            //release buffer
            unmanagedAlignedBuffer.Release();

        }

        Object busyRenderingLock = new object();
        private void bgwContinuousAcquire_ProgressChanged(Object sender, ProgressChangedEventArgs ev) {
            //continuousAcquireBusyRendering = true;

            NewFrameArrivedEventArgs e = (NewFrameArrivedEventArgs)ev.UserState;
            NewFrameArrived?.Invoke(this, e); //raise an event
            //e = null;  //cleanup, we can clean safely because event is fired in same thread, when we reach here means the event end, and the event client is finish using this
            //ev = null;

            //continuousAcquireBusyRendering = false;
        }

        private void bgwContinuousAcquire_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {

        }

        // UNDER CONSTRUCTION
        //** continuous acquire using Task (for .NET >= 4.5) 
                private CancellationTokenSource _tokenSource = null;


        public async Task AcquireContinuousAsync(IProgress<NewFrameArrivedEventArgs> iProgress)
        {
            this.IsAcquiring = true;
            //            var token = _tokenSource.Token;
            _tokenSource = new CancellationTokenSource();
            var token = _tokenSource.Token;
            //settings
            SetTriggerMode(TriggerMode.SoftWare);
            SetCycleMode(CycleMode.Continuous);
            UpdateResultImageParameters();
            
            //queue buffers
            int numberOfBuffers = 30;
            var unmanagedAlignedBuffer = new UnmanagedAlignedBuffers(numberOfBuffers, cameraHandle, imageByteCount);
            managedBuffers = new ManagedBuffers(numberOfBuffers, AOIWidth * AOIHeight);
            //acquire
            int frameIndex = 0;
            ATCore.AT_Command(cameraHandle, "Acquisition Start");

            await Task.Run(() =>
            {
                while (true)
                {
                    // task cancellation
                    if (token.IsCancellationRequested)
                    {
                        _tokenSource.Dispose();
                        _tokenSource = null;
                        break;
                    }

                    frameIndex++;
                    var ev = new NewFrameArrivedEventArgs(frameIndex
                                                             , CaptureFrame(timeout: 100000));
                    // raise event
                    //NewFrameArrived?.Invoke(this, ev);
                    iProgress.Report(ev);

                    unmanagedAlignedBuffer.QueueNextBuffer();
                    managedBuffers.QueueNextBuffer();
                }
            }, token);

            int stopSucess = ATCore.AT_Command(cameraHandle, "AcquisitionStop");
            ATCore.AT_Flush(cameraHandle);
            RestartConnection(); //prevent simcam cpu eating bug

            //release buffer
            unmanagedAlignedBuffer.Release();
            this.IsAcquiring = false;
        }

        public async Task AcquireContinuousAsyncDUMMY(IProgress<NewFrameArrivedEventArgs> iProgress)
        {
            // NOTHING IS TO BE DONE
        }
        public void StopContinuousAcquisition()
        {
            if (_tokenSource != null) _tokenSource.Cancel();
        }

        //public delegate void NewFrameArrivedEventHandler(object sender, NewFrameArrivedEventArgs e);
        //public event NewFrameArrivedEventHandler NewFrameArrived;
        public event EventHandler<NewFrameArrivedEventArgs> NewFrameArrived;
        public class NewFrameArrivedEventArgs : EventArgs {
            public int frameIndex { get; set; }
            public ushort[] frameData { get; set; }
            public NewFrameArrivedEventArgs(int frameIndex, ushort[] frameData) {
                this.frameIndex = frameIndex;
                this.frameData = frameData;
            }
        }
    }
}