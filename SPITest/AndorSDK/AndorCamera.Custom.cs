﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Threading;
using Vector = System.Windows.Vector;
using BackgroundWorker = System.ComponentModel.BackgroundWorker;
using DoWorkEventArgs = System.ComponentModel.DoWorkEventArgs;
using ProgressChangedEventArgs = System.ComponentModel.ProgressChangedEventArgs;
using RunWorkerCompletedEventArgs = System.ComponentModel.RunWorkerCompletedEventArgs;

using WpfApp.ImageProcessingTools;
using DeflectorSystem.GalvoScanner.GalvoScanner2D;
using Point2f = OpenCvSharp.Point2f;

using DeflectorSystem.GalvoScanner.GalvoScanner2D;

// Programs made only for specific use and modules under examination.
namespace AndorSDK
{
    public partial class AndorCamera : IDisposable
    {
        private ImageProcessing.ProcessPrms prms;

        private async void bgwContinuousAcquire_DoWorkCustom(Object sender, DoWorkEventArgs e)
        {
            //settings
            SetTriggerMode(TriggerMode.SoftWare);
            SetCycleMode(CycleMode.Continuous);
            UpdateResultImageParameters();
            //MessageBox.Show(pixelEncoding.encoding + " " + imageByteCount.ToString() + " " + AOILeft.ToString() + " " + AOITop.ToString() + " " + AOIWidth.ToString() + " " + AOIHeight.ToString() + " " + AOIStride.ToString() + " " + AOIBinning.mode + " " + frameRate.ToString());

            //queue buffers
            int numberOfBuffers = 30;
            var unmanagedAlignedBuffer = new UnmanagedAlignedBuffers(numberOfBuffers, cameraHandle, imageByteCount);
            managedBuffers = new ManagedBuffers(numberOfBuffers, AOIWidth * AOIHeight);

            //acquire
            int frameIndex = 0;
            ATCore.AT_Command(cameraHandle, "Acquisition Start");

            ushort[] img, img_new;

            Task imageProcessTask;
            Task<ushort[]> getImageTask;

            img = CaptureFrame(timeout: 100000);

            Point2f center;

            while (true)
            {
                if (bgwContinuousAcquire.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                frameIndex++;
                var ev = new NewFrameArrivedEventArgs(frameIndex, img);

                //if (!continuousAcquireBusyRendering) { //owner ready to recieve frame
                //continuousAcquireBusyRendering = true;
                bgwContinuousAcquire.ReportProgress(0, ev);
                //}

                getImageTask = Task.Run<ushort[]>(() =>
                {
                    ushort[] imageData = CaptureFrame(timeout: 100000);

                    // Not sure where to put these two lines.
                    unmanagedAlignedBuffer.QueueNextBuffer();
                    managedBuffers.QueueNextBuffer();
                    return imageData;
                });

                imageProcessTask = Task.Run(() =>
                {
                    // the use of center is limited in this constructor.
                    center = ImageProcessing.GetCenterOfMass(img, this.AOIHeight, this.AOIWidth, prms).centerOfMass;
                    // Do some action related to galvo scanner.
                });

                // wait for all threads to complete.
                img_new = (await getImageTask);
                await imageProcessTask;
                img = img_new;
            }

            //MessageBox.Show("test");

            int stopSucess = ATCore.AT_Command(cameraHandle, "AcquisitionStop");
            ATCore.AT_Flush(cameraHandle);
            RestartConnection(); //prevent simcam cpu eating bug

            //release buffer
            unmanagedAlignedBuffer.Release();
        }

        public async Task AcquireContinuousAsyncCustom(IProgress<NewFrameArrivedEventArgsCustom> iProgress)
        {
            this.IsAcquiring = true;
            //            var token = _tokenSource.Token;
            _tokenSource = new CancellationTokenSource();
            var token = _tokenSource.Token;
            //settings
            SetTriggerMode(TriggerMode.SoftWare);
            SetCycleMode(CycleMode.Continuous);
            UpdateResultImageParameters();

            //queue buffers
            int numberOfBuffers = 30;
            var unmanagedAlignedBuffer = new UnmanagedAlignedBuffers(numberOfBuffers, cameraHandle, imageByteCount);
            managedBuffers = new ManagedBuffers(numberOfBuffers, AOIWidth * AOIHeight);
            //acquire
            ushort[] img, img_new;

            Task imageProcessTask;
            Task<ushort[]> getImageTask;

            int frameIndex = 0;
            ATCore.AT_Command(cameraHandle, "Acquisition Start");

            img = CaptureFrame(timeout: 100000);
            ImageProcessing.ProcessedData processedData;

            if(getCenterOfMass != null)
            {
                processedData = getCenterOfMass(img, this.AOIHeight, this.AOIWidth);
            } else
            {
                processedData = new ImageProcessing.ProcessedData();
            }

            await Task.Run(async () =>
            {
                while (true)
                {
                    // task cancellation
                    if (token.IsCancellationRequested)
                    {
                        _tokenSource.Dispose();
                        _tokenSource = null;
                        break;
                    }

                    frameIndex++;
                    var ev = new NewFrameArrivedEventArgsCustom(frameIndex, img, processedData);
                    ev.imageHeight = this.AOIHeight;
                    ev.imageWidth = this.AOIWidth;
                    // raise event
                    iProgress.Report(ev);

                    getImageTask = Task.Run<ushort[]>(() =>
                    {
                        ushort[] imageData = CaptureFrame(timeout: 100000);

                        // Not sure where to put these two lines.
                        unmanagedAlignedBuffer.QueueNextBuffer();
                        managedBuffers.QueueNextBuffer();
                        return imageData;
                    });

                    imageProcessTask = Task.Run(() =>
                    {
                        if(getCenterOfMass != null)
                        {
                            processedData = getCenterOfMass(img, this.AOIHeight, this.AOIWidth);
                            // Do some action related to galvo scanner.
                            updateFunc(processedData.centerOfMass, img, frameIndex);
                        } else
                        {
                            // we dont use center of mass if getCenterOfMass is unassigned.
                            updateFunc(new Point2f(0,0), img, frameIndex);
                        }                        
                    });

                    // wait for all threads to complete.
                    img_new = (await getImageTask);
                    await imageProcessTask;
                    img = img_new;
                }
            }, token);

            int stopSucess = ATCore.AT_Command(cameraHandle, "AcquisitionStop");
            ATCore.AT_Flush(cameraHandle);
            RestartConnection(); //prevent simcam cpu eating bug

            //release buffer
            unmanagedAlignedBuffer.Release();
            this.IsAcquiring = false;
        }

        public class NewFrameArrivedEventArgsCustom : EventArgs
        {
            public int imageHeight { get; set;}
            public int imageWidth { get; set; }
            public int frameIndex { get; set; }
            public ushort[] frameData { get; set; }
            public ImageProcessing.ProcessedData processedData { get; set; }

            public NewFrameArrivedEventArgsCustom(int frameIndex, ushort[] frameData, ImageProcessing.ProcessedData processedData)
            {
                this.frameIndex = frameIndex;
                this.frameData = frameData;
                this.processedData = processedData;
            }
        }

        public delegate void UpdateScannerAngle(Point2f center, ushort[] imageData, int frameCount);
        public UpdateScannerAngle updateFunc;

        public delegate ImageProcessing.ProcessedData GetCenterOfMassMethod(ushort[] imageData, int AOIHeight, int AOIWidth);
        public GetCenterOfMassMethod getCenterOfMass;
    }
}
