﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Runtime.InteropServices;
using Stopwatch = System.Diagnostics.Stopwatch;
using Parallel = System.Threading.Tasks.Parallel;

namespace AndorSDK {
    //*** this is test methods, do not use in real production
    public partial class AndorCamera : IDisposable {

        [Obsolete]
        public Bitmap AcquireSingle(int imageByteCount, int AOIwidth, int AOIheight) {
            Bitmap bmp = new Bitmap(1, 1);

            //MessageBox.Show($"bytecount:{imageByteCount.ToString()} AOIwidth:{AOIwidth.ToString()} AOIheight{AOIheight.ToString()}");

            //queue buffer, we need pointer to unmanaged allocated unsign char array (in C# it is byte[])
            GCHandle pinnedArrayUserBuffer = GCHandle.Alloc(new byte[imageByteCount], GCHandleType.Pinned);
            IntPtr userBuffer = pinnedArrayUserBuffer.AddrOfPinnedObject();
            ATCore.AT_QueueBuffer(cameraHandle, userBuffer, imageByteCount);


            ATCore.AT_Command(cameraHandle, "Acquisition Start");

            //MessageBox.Show("waiting for acquisition");

            IntPtr buffer = new IntPtr();
            int acquisitionSuccess = ATCore.AT_WaitBuffer(cameraHandle, ref buffer, ref imageByteCount, 10000);
            if (acquisitionSuccess == ATCore.AT_SUCCESS) {


                //this is not slow
                var pixelValues = new byte[AOIwidth * AOIheight];
                for (int i = 0; i < imageByteCount; i += 3) {
                    byte byte0 = Marshal.ReadByte(buffer, i);
                    byte byte1 = Marshal.ReadByte(buffer, i + 1);
                    byte byte2 = Marshal.ReadByte(buffer, i + 2);

                    int LowPixel = (byte0 << 4) + (byte1 & 0xF);
                    int HighPixel = (byte2 << 4) + (byte1 >> 4);

                    pixelValues[i / 3 * 2] = (byte)(LowPixel * 256 / 4096);
                    pixelValues[i / 3 * 2 + 1] = (byte)(HighPixel * 256 / 4096);
                }

                bmp = AndorSDK.Imaging.Generate8bbpBitmapByData(AOIwidth, AOIheight, pixelValues);
                //bmp = new Bitmap(AOIwidth, AOIheight); this is not slow
                //MessageBox.Show(pixelValues[30].ToString() + " " + pixelValues[70].ToString());

            }

            //Free the allocated buffer
            pinnedArrayUserBuffer.Free(); //this is not so slow

            ATCore.AT_Command(cameraHandle, "Acquisition Stop");
            ATCore.AT_Flush(cameraHandle);



            return bmp;
        }

        [Obsolete]
        public byte[] Acquire2(int imageByteCount) {
            var result = new byte[imageByteCount];

            //queue buffer, we need pointer to unmanaged memory
            IntPtr userBuffer = Marshal.AllocHGlobal(imageByteCount);
            ATCore.AT_QueueBuffer(cameraHandle, userBuffer, imageByteCount);


            ATCore.AT_Command(cameraHandle, "Acquisition Start");

            result = OldCaptureFrame(imageByteCount, 10000U);

            //Free the allocated buffer
            Marshal.FreeHGlobal(userBuffer);

            ATCore.AT_Command(cameraHandle, "Acquisition Stop");
            ATCore.AT_Flush(cameraHandle);

            return result;
        }

        [Obsolete]
        private byte[] OldCaptureFrame(int imageByteCount, uint timeout) {
            var result = new byte[imageByteCount];
            IntPtr buffer = new IntPtr();
            int acquisitionSuccess = ATCore.AT_WaitBuffer(cameraHandle, ref buffer, ref imageByteCount, timeout);
            if (acquisitionSuccess == ATCore.AT_SUCCESS) {
                Marshal.Copy(buffer, result, 0, imageByteCount);
            }
            return result;
        }

        //private int[] CaptureFrame2(int imageByteCount, int AOIWidth, int AOIHeight,
        //                            int AOIStride, uint timeout) {
        //    var result = new int[AOIWidth * AOIHeight];
        //    IntPtr buffer = new IntPtr();
        //    //recieve frame
        //    int acquisitionSuccess = ATCore.AT_WaitBuffer(cameraHandle, ref buffer, ref imageByteCount, timeout);
        //    if (acquisitionSuccess == ATCore.AT_SUCCESS) {

        //        //on-the-fly convert in unmanaged memory using ATUtil
        //        IntPtr unpackBuffer = Marshal.AllocHGlobal(AOIWidth * AOIHeight * 32);
        //        ATUtil.AT_ConvertBuffer(buffer, unpackBuffer, AOIWidth, AOIHeight, AOIStride
        //                                , PixelEncoding.Mono12Packed.encoding
        //                                , PixelEncoding.Mono32.encoding);

        //        Marshal.Copy(unpackBuffer, result, 0, result.Length);
        //        Marshal.FreeHGlobal(unpackBuffer);
        //    }
        //    return result;
        //}

        //public List<uint[]> AcquireMulti2(int frameCount) {
        //    var dataList = new List<uint[]>();

        //    SetTriggerMode(TriggerMode.SoftWare);
        //    SetCycleMode(CycleMode.Continuous);

        //    UpdateResultImageParameters();

        //    //queue buffers
        //    int bufferCount = 30;
        //    List<IntPtr> AcqBuffers = new List<IntPtr>();
        //    List<IntPtr> AlignedBuffers = new List<IntPtr>();
        //    for (int i = 0; i < bufferCount; i++) {
        //        IntPtr newAcqBuffer = Marshal.AllocHGlobal(imageByteCount + 7);
        //        AcqBuffers.Add(newAcqBuffer);
        //        IntPtr newAlignedBuffer = new IntPtr((newAcqBuffer + 7).ToInt64() & (~(7L)));
        //        AlignedBuffers.Add(newAlignedBuffer);
        //        ATCore.AT_QueueBuffer(cameraHandle, newAlignedBuffer, imageByteCount);
        //    }

        //    //acquire
        //    ATCore.AT_Command(cameraHandle, "Acquisition Start");
        //    for (int frameIndex = 0; frameIndex < frameCount; frameIndex++) {
        //        dataList.Add(
        //            CaptureFrame2(timeout: 10000));
        //        ATCore.AT_QueueBuffer(
        //            cameraHandle, AlignedBuffers[frameIndex % bufferCount], _imageByteCount);

        //    }
        //    ATCore.AT_Command(cameraHandle, "AcquisitionStop");
        //    ATCore.AT_Flush(cameraHandle);

        //    //release buffer
        //    for (int i = 0; i < bufferCount; i++) {
        //        Marshal.FreeHGlobal(AcqBuffers[i]);
        //    }

        //    return dataList;
        //}

        ////** use this only in background task
        //private void AcquireContinuousTask(Object sender
        //                                   , CancellationToken cancellationToken) {
        //    //settings
        //    SetTriggerMode(TriggerMode.SoftWare);
        //    SetCycleMode(CycleMode.Continuous);
        //    UpdateResultImageParameters();

        //    //queue buffers
        //    int numberOfBuffers = 10;
        //    var alignedBuffer = new AlignedBuffers(numberOfBuffers, cameraHandle, imageByteCount);
        //    alignedBuffer.QueueAll();

        //    //acquire
        //    int frameIndex = 0;
        //    ATCore.AT_Command(cameraHandle, "Acquisition Start");
        //    while (true) {
        //        if (cancellationToken.IsCancellationRequested) {
        //            break;
        //        }

        //        frameIndex++;
        //        var e = new NewFrameArrivedEventArgs(frameIndex
        //                                             , CaptureFrame(timeout: 10000));
        //        if (!continuousAcquireBusyRendering) { //owner ready to recieve frame
        //            ((Form)sender).BeginInvoke(NewFrameArrived, new object[] { this, e });
        //        }

        //        //NewFrameArrivedInvoker(e);
        //        //NewFrameArrived(this, e);

        //        alignedBuffer.QueueNextBuffer();

        //    }
        //    ATCore.AT_Command(cameraHandle, "AcquisitionStop");
        //    ATCore.AT_Flush(cameraHandle);

        //    //release buffer
        //    alignedBuffer.Release();
        //}

        //private CancellationTokenSource tokenSourceContinuousAcquire;
        //public void AcquireContinuousStart(Object sender) {
        //    tokenSourceContinuousAcquire = new CancellationTokenSource();
        //    var token = tokenSourceContinuousAcquire.Token;
        //    Task.Factory.StartNew(() => AcquireContinuousTask(sender, token));
        //}

        //public void AcquireContinuousStop() {
        //    tokenSourceContinuousAcquire.Cancel();
        //}

        //public string GetSerial() {
        //    GCHandle pinnedArraySerialPointer = GCHandle.Alloc(new byte[64], GCHandleType.Pinned);
        //    IntPtr serialStringPointer = pinnedArraySerialPointer.AddrOfPinnedObject();
        //    int getSerialSuccess = ATCore.AT_GetString(cameraHandle, "Serial Number", serialStringPointer, 64);
        //    string serialString = Marshal.PtrToStringUni(serialStringPointer);
        //    pinnedArraySerialPointer.Free();


        //    if (getSerialSuccess == ATCore.AT_SUCCESS) {
        //        return serialString;
        //    }
        //    else {
        //        return "ERROR";
        //    }
        //}
    }
}
