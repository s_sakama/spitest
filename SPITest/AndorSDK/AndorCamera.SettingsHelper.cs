﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace AndorSDK {
    //*** helper methods for get/set settings
    public partial class AndorCamera : IDisposable {
        /// <summary>get setting of type bool (see <c>SettingName</c>), return success code</summary>
        public int GetBool(string featureName, out bool resultBool) {
            int result = 0;
            int getBoolSuccess = ATCore.AT_GetBool(
                                     cameraHandle, featureName, ref result);

            if (result == ATCore.AT_TRUE) {
                resultBool = true;
            }
            else {
                resultBool = false;
            }

            return getBoolSuccess;
        }


        /// <summary>get setting of type int (see <c>SettingName</c>), return success code</summary>
        public int GetInt(string featureName, out long resultInt) {
            long result = 0;
            int getIntSuccess = ATCore.AT_GetInt(
                                     cameraHandle, featureName, ref result);
            resultInt = result;
            return getIntSuccess;
        }

        /// <summary>get setting of type float (see <c>SettingName</c>), return success code</summary>
        public int GetFloat(string featureName, out double resultDouble) {
            double result = 0;
            int getFloatSuccess = ATCore.AT_GetFloat(
                                     cameraHandle, featureName, ref result);
            resultDouble = result;
            return getFloatSuccess;
        }

        /// <summary>get setting of type string (see <c>SettingName</c>), return success code</summary>
        public int GetString(string featureName, out string resultString) {
            //use GCHandle pinning because Marshal.AllocHGlobal is not working for unknown reason
            GCHandle pinnedArrayTargetStringPointer = GCHandle.Alloc(new byte[64], GCHandleType.Pinned);
            IntPtr targetStringPointer = pinnedArrayTargetStringPointer.AddrOfPinnedObject();
            int successCode = ATCore.AT_GetString(cameraHandle, featureName, targetStringPointer, 64);
            resultString = Marshal.PtrToStringUni(targetStringPointer);
            pinnedArrayTargetStringPointer.Free();
            return successCode;
        }

        /// <summary>get setting of type enumerated (see <c>SettingName</c>), return success code</summary>
        public int GetEnumerateString(string featureName, out string resultString) {
            int currentEnumIndex = 0;
            int successCode = ATCore.AT_GetEnumIndex(cameraHandle, featureName, ref currentEnumIndex);
            if (successCode != ATCore.AT_SUCCESS) {
                resultString = "ERROR: cannot get index";
                return successCode;
            }

            //use GCHandle pinning because Marshal.AllocHGlobal is not working for unknown reason
            GCHandle pinnedArrayTargetStringPointer = GCHandle.Alloc(new byte[64], GCHandleType.Pinned);
            IntPtr targetStringPointer = pinnedArrayTargetStringPointer.AddrOfPinnedObject();
            int successCode2 = ATCore.AT_GetEnumeratedString(cameraHandle, featureName, currentEnumIndex, targetStringPointer, 64);
            if (successCode2 != ATCore.AT_SUCCESS) {
                resultString = "ERROR: cannot get enumerated string";
                return successCode2;
            }

            resultString = Marshal.PtrToStringUni(targetStringPointer);
            pinnedArrayTargetStringPointer.Free();
            return successCode2;
        }

        /// <summary> show all enumerated values of setting type enumerated (see <c>SettingName</c>)</summary>
        public int GetListEnumerateValues(string featureName, out string allEnumerateValues) {
            allEnumerateValues = "";

            int enumCount = 0;
            int getEnumCountSuccess = ATCore.AT_GetEnumCount(cameraHandle, featureName, ref enumCount);
            if (getEnumCountSuccess != ATCore.AT_SUCCESS) {
                allEnumerateValues = "ERROR: cannot get enum count";
                return getEnumCountSuccess;
            }

            for (int i = 0; i < enumCount; i++) {
                //get enum string
                GCHandle pinnedArrayTargetStringPointer = GCHandle.Alloc(new byte[72], GCHandleType.Pinned);
                IntPtr targetStringPointer = pinnedArrayTargetStringPointer.AddrOfPinnedObject();
                ATCore.AT_GetEnumeratedString(cameraHandle, featureName, i, targetStringPointer, 72);
                string resultString = Marshal.PtrToStringUni(targetStringPointer);
                pinnedArrayTargetStringPointer.Free();

                //check availabality
                int intAvailable = 0;
                ATCore.AT_IsEnumIndexAvailable(cameraHandle, featureName, i, ref intAvailable);
                if (intAvailable == ATCore.AT_FALSE) {
                    resultString += " (not available)";
                }

                //check implemented
                int intImplemented = 0;
                ATCore.AT_IsEnumIndexImplemented(cameraHandle, featureName, i, ref intImplemented);
                if (intImplemented == ATCore.AT_FALSE) {
                    resultString += " (not implemented)";
                }

                allEnumerateValues += resultString + Environment.NewLine;
            }

            return ATCore.AT_SUCCESS;
        }

        /// <summary>set setting of type bool (see <c>SettingName</c>), return success code</summary>
        public int SetBool(string featureName, bool boolValue) {
            int intValue;
            if (boolValue) {
                intValue = ATCore.AT_TRUE;
            }
            else {
                intValue = ATCore.AT_FALSE;
            }

            int successCode = ATCore.AT_SetBool(
                  cameraHandle, featureName, intValue);
            return successCode;
        }

        /// <summary>set setting of type int (see <c>SettingName</c>), return success code</summary>
        public int SetInt(string featureName, int intValue) {
            int successCode = ATCore.AT_SetInt(
                  cameraHandle, featureName, intValue);
            return successCode;
        }

        /// <summary>set setting of type float (see <c>SettingName</c>), return success code</summary>
        public int SetFloat(string featureName, double doubleValue) {
            int successCode = ATCore.AT_SetFloat(
                  cameraHandle, featureName, doubleValue);
            return successCode;
        }

        /// <summary>set setting of type string (see <c>SettingName</c>), return success code</summary>
        public int SetString(string featureName, string stringValue) {
            int successCode = ATCore.AT_SetString(
                  cameraHandle, featureName, stringValue);
            return successCode;
        }

        /// <summary>set setting of type enumerated (see <c>SettingName</c>), return success code</summary>
        public int SetEnumeratedString(string featureName, string stringValue) {
            int successCode = ATCore.AT_SetEnumeratedString(
                  cameraHandle, featureName, stringValue);
            return successCode;
        }
    }
}
