﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;

using Marshal = System.Runtime.InteropServices.Marshal;
using Parallel = System.Threading.Tasks.Parallel;
using ParallelOptions = System.Threading.Tasks.ParallelOptions;
using System.Runtime.InteropServices;


namespace AndorSDK {
    public static class Imaging {

        public static void GetMaxMin(ushort[] data, out ushort max, out ushort min) {
            max = ushort.MinValue;
            min = ushort.MaxValue;

            for (int i = 0; i < data.Length; i++) {
                ushort value = data[i];
                if (value > max) { max = value; }
                if (value < min) { min = value; }
            }
        }

        /// <summary>8bppIndexed</summary>
        public static byte[] GenerateTableDataToColor8bpp
            (ushort rangeMax, ushort rangeMin) {
            var resultTable = new byte[ushort.MaxValue + 1];
            ushort range = (ushort) (rangeMax - rangeMin);
            if(range == 0) { range = 1; }

            for(int i=0; i<rangeMin; i++) {
                resultTable[i] = 0;
            }
            for(int i=rangeMin; i<rangeMax; i++) {
                resultTable[i] = (byte) ((255.0 * (i - rangeMin)) / range);
            }
            for(int i=rangeMax; i < resultTable.Length; i++) {
                resultTable[i] = 255;
            }

            return resultTable;
        }


        /// <summary>32bppArgb</summary>
        public static uint[] GenerateTableDataToColor32bpp(ushort rangeMax, ushort rangeMin) {
            const uint ALPHA = 255, a = 256 * 256 * 256, r = 256 * 256, g = 256, b = 1;
            var resultTable = new uint[ushort.MaxValue + 1];
            ushort range = (ushort)(rangeMax - rangeMin);
            if (range == 0) { range = 1; }

            for (int i = 0; i < rangeMin; i++) {
                resultTable[i] = ALPHA*a + 0*r + 0*g + 0*b;
                //resultTable[i] = uint.MaxValue - 255*256;
            }
            for (int i = rangeMin; i < rangeMax; i++) {
                uint colorVal = (uint)((255.0 * (i - rangeMin)) / range);
                resultTable[i] = ALPHA*a + colorVal*r + colorVal*g + colorVal*b;
                //resultTable[i] = uint.MaxValue - 255*256;
            }
            for (int i = rangeMax; i < resultTable.Length; i++) {
                resultTable[i] = ALPHA*a + 255U*r + 255U*g + 255U*b;
                //resultTable[i] = uint.MaxValue - 255*256;
            }

            return resultTable;
        }


        /// <summary>8bppIndexed</summary>
        public static Bitmap Generated8bppBitmapByTable(int width, int height, ushort[] data, byte[] tableDataToColor) {
            var generatedBitmap = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
            var bitmapDataInMemory = generatedBitmap.LockBits(new Rectangle(0, 0, width, height),
                                                                System.Drawing.Imaging.ImageLockMode.ReadWrite,
                                                                generatedBitmap.PixelFormat);

            const int chunkCount = 8;
            var prallelOptions = new ParallelOptions { MaxDegreeOfParallelism = chunkCount };

            Parallel.For(0, chunkCount, prallelOptions, j => {
                int startIndex = j * (height / chunkCount);
                int stopIndex;
                if (j == chunkCount - 1) { //final chunk
                    stopIndex = height;
                }
                else {
                    stopIndex = (j + 1) * (height / chunkCount);
                }

                unchecked {
                    unsafe {
                        fixed (ushort* pSource = data) {
                            fixed (byte* pTable = tableDataToColor) {
                                ushort* pS = pSource + startIndex * width;
                                for (int y = startIndex; y < stopIndex; y++) {
                                    byte* pT = (byte*)(bitmapDataInMemory.Scan0 + (y * bitmapDataInMemory.Stride));
                                    for (int x = 0; x < bitmapDataInMemory.Width; x++) {
                                        *pT = *(pTable + *pS);
                                        pS++;
                                        pT++;
                                    }
                                }
                            }
                        }

                    }
                }
            });

            generatedBitmap.UnlockBits(bitmapDataInMemory);
            SetGreyScalePallete(ref generatedBitmap);
            return generatedBitmap;
        }

        /// <summary>32bppArgb</summary>
        public static Bitmap Generated32bppBitmapByTable(int width, int height, ushort[] data, uint[] tableDataToColor) {
            var generatedBitmap = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
            var bitmapDataInMemory = generatedBitmap.LockBits(new Rectangle(0, 0, width, height),
                                                                System.Drawing.Imaging.ImageLockMode.ReadWrite,
                                                                generatedBitmap.PixelFormat);

            const int chunkCount = 8;
            var prallelOptions = new ParallelOptions { MaxDegreeOfParallelism = chunkCount };

            Parallel.For(0, chunkCount, prallelOptions,j => {
                int startIndex = j * (height / chunkCount);
                int stopIndex;
                if (j == chunkCount - 1) { //final chunk
                    stopIndex = height;
                }
                else {
                    stopIndex = (j + 1) * (height / chunkCount);
                }

                unchecked {
                    unsafe {
                        fixed (ushort* pSource = data) {
                            fixed (uint* pTable = tableDataToColor) {
                                ushort* pS = pSource + startIndex*width;
                                for (int y = startIndex; y < stopIndex; y++) {
                                    uint* pT = (uint*)(bitmapDataInMemory.Scan0 + (y * bitmapDataInMemory.Stride));
                                    for (int x = 0; x < bitmapDataInMemory.Width; x++) {
                                        *pT = *(pTable + *pS);
                                        pS++;
                                        pT++;
                                    }
                                }
                            }
                        }

                    }
                }
            });

            generatedBitmap.UnlockBits(bitmapDataInMemory);
            return generatedBitmap;
        }

        private static ColorPalette grey256pallete { get; set; }
        private static void SetGreyScalePallete(ref Bitmap b) {
            if (grey256pallete != null) {
                //pallete exist, use it
                b.Palette = grey256pallete;
            }
            else {
                //pallete not create, create greyscale pallete
                var tempBitmap = new Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
                grey256pallete = tempBitmap.Palette;
                Color[] entries = grey256pallete.Entries;
                for (int i = 0; i < 256; i++) {
                    entries[i] = Color.FromArgb(i, i, i);
                }
                b.Palette = grey256pallete;
            }

        }



        #region obsolete

        private static byte[] rescaleResult8bpp;
        [Obsolete("use GeneratedBitmap8bppByTable instead")]
        public static byte[] RescaleTo8bppByTable(ushort[] data, byte[] tableDataToColor) {
            //var result = new byte[data.Length]; //big array allocation is slow, use out-of-scope variable instead
            if (rescaleResult8bpp == null) {
                rescaleResult8bpp = new byte[data.Length];
            }
            else if (rescaleResult8bpp.Length != data.Length) {
                rescaleResult8bpp = new byte[data.Length];
            }

            int chunkCount = 16;
            Parallel.For(0, chunkCount, j => {
                int startIndex = j * (data.Length / chunkCount);
                int stopIndex;
                if (j == chunkCount - 1) { //final chunk
                    stopIndex = data.Length;
                }
                else {
                    stopIndex = (j + 1) * (data.Length / chunkCount);
                }

                unchecked {
                    unsafe {
                        fixed (ushort* pSource = data) {
                            fixed (byte* pTarget = rescaleResult8bpp) {
                                fixed (byte* pTable = tableDataToColor) {
                                    ushort* pS = pSource + startIndex;
                                    byte* pT = pTarget + startIndex;

                                    for (int index = startIndex; index < stopIndex; index++) {
                                        //for (int i = stopIndex - startIndex; i > 0 ; i--) {
                                        //*pT = tableDataToColor[*pS];
                                        //*(pTarget+index) = *(pTable + *(pSource+index));
                                        *pT = *(pTable + *pS);
                                        pS++;
                                        pT++;
                                    }
                                }
                            }
                        }
                    }
                }

            });

            return rescaleResult8bpp;
        }

        private static uint[] rescaleResult32bpp;
        [Obsolete("use GeneratedBitmap32bppPArgbByTable instead")]
        public static uint[] RescaleTo32bppByTable(ushort[] data, uint[] tableDataToColor) {
            //var result = new byte[data.Length]; //big array allocation is slow, use out-of-scope variable instead
            if (rescaleResult32bpp == null) {
                rescaleResult32bpp = new uint[data.Length];
            }
            else if (rescaleResult32bpp.Length != data.Length) {
                rescaleResult32bpp = new uint[data.Length];
            }

            int chunkCount = 16;
            Parallel.For(0, chunkCount, j => {
                int startIndex = j * (data.Length / chunkCount);
                int stopIndex;
                if (j == chunkCount - 1) { //final chunk
                    stopIndex = data.Length;
                }
                else {
                    stopIndex = (j + 1) * (data.Length / chunkCount);
                }

                unchecked {
                    unsafe {
                        fixed (ushort* pSource = data) {
                            fixed (uint* pTarget = rescaleResult32bpp) {
                                fixed (uint* pTable = tableDataToColor) {
                                    ushort* pS = pSource + startIndex;
                                    uint* pT = pTarget + startIndex;

                                    for (int index = startIndex; index < stopIndex; index++) {
                                        //for (int i = stopIndex - startIndex; i > 0 ; i--) {
                                        //*pT = tableDataToColor[*pS];
                                        //*(pTarget+index) = *(pTable + *(pSource+index));
                                        *pT = *(pTable + *pS);
                                        pS++;
                                        pT++;
                                    }
                                }
                            }
                        }
                    }
                }

            });

            return rescaleResult32bpp;
        }

        [Obsolete("use GeneratedBitmap32bppPArgbByTable instead")]
        public static Bitmap Generate8bbpBitmapByData(int width, int height, byte[] values) {
            var generatedBitmap = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
            var bitmapDataInMemory = generatedBitmap.LockBits(new Rectangle(0, 0, width, height),
                                                                System.Drawing.Imaging.ImageLockMode.ReadWrite,
                                                                generatedBitmap.PixelFormat);


            Marshal.Copy(values, 0, bitmapDataInMemory.Scan0, width * height);

            generatedBitmap.UnlockBits(bitmapDataInMemory);
            SetGreyScalePallete(ref generatedBitmap);

            return generatedBitmap;
        }

        [Obsolete("use GeneratedBitmap32bppPArgbByTable instead")]
        public static Bitmap Generate32bppBitmapByData(int width, int height, uint[] values) {
            var generatedBitmap = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
            var bitmapDataInMemory = generatedBitmap.LockBits(new Rectangle(0, 0, width, height),
                                                                System.Drawing.Imaging.ImageLockMode.ReadWrite,
                                                                generatedBitmap.PixelFormat);


            //Marshal.Copy(values, 0, bitmapDataInMemory.Scan0, width * height);
            unsafe {
                fixed (uint* ptrSource = &values[0]) {
                    CopyMemory(bitmapDataInMemory.Scan0, (IntPtr)ptrSource, (uint)(values.Length * 4));
                }
            }

            generatedBitmap.UnlockBits(bitmapDataInMemory);

            return generatedBitmap;
        }

        [DllImport("kernel32.dll", EntryPoint = "RtlMoveMemory", SetLastError = false)]
        static extern void CopyMemory(IntPtr Destination, IntPtr Source, uint Length);

        

        /// <summary> Generate a bitmap pixel-by-pixel by red,green,blue byte arrays. Using unsafe code
        /// to directly manipulate pixel in memory. Returns the generated bitmap.</summary>
        [Obsolete("use DataToNormalizedBitmap instead")]
        public static Bitmap Generate24bppBitMapByPixel(int width, int height, byte[] red, byte[] green, byte[] blue) {
            var generatedBitmap = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            var bitmapDataInMemory = generatedBitmap.LockBits(new Rectangle(0, 0, width, height),
                                                                System.Drawing.Imaging.ImageLockMode.ReadWrite,
                                                                generatedBitmap.PixelFormat);
            const int PIXELSIZEINBYTE = 3;
            int i;
            unsafe {
                for (int y = 0; y < bitmapDataInMemory.Height; y++) {
                    byte* row = (byte*)bitmapDataInMemory.Scan0 + (y * bitmapDataInMemory.Stride);
                    for (int x = 0; x < bitmapDataInMemory.Width; x++) {
                        i = y * width + x;
                        row[x * PIXELSIZEINBYTE] = blue[i];   //Blue  0-255
                        row[x * PIXELSIZEINBYTE + 1] = green[i]; //Green 0-255
                        row[x * PIXELSIZEINBYTE + 2] = red[i];   //Red   0-255
                    }
                }
            }
            generatedBitmap.UnlockBits(bitmapDataInMemory);
            return generatedBitmap;
        }

        [Obsolete("use DataToNormalizedBitmap instead")]
        public static Bitmap Generate32bppBitmapByPixel(int width, int height, byte[] data) {
            var generatedBitmap = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
            var bitmapDataInMemory = generatedBitmap.LockBits(new Rectangle(0, 0, width, height),
                                                                System.Drawing.Imaging.ImageLockMode.ReadWrite,
                                                                generatedBitmap.PixelFormat);
            const byte ALPHA = 255;
            const int PIXELSIZEINBYTE = 4;
            unsafe {
                int chunkCount = 16;
                Parallel.For(0, chunkCount, j => {
                    int startIndex = j * (bitmapDataInMemory.Height / chunkCount);
                    int stopIndex;
                    if (j == chunkCount - 1) { //final chunk
                        stopIndex = bitmapDataInMemory.Height;
                    }
                    else {
                        stopIndex = (j + 1) * (bitmapDataInMemory.Height / chunkCount);
                    }

                    for (int y = startIndex; y < stopIndex; y++) {
                        byte* row = (byte*)bitmapDataInMemory.Scan0 + (y * bitmapDataInMemory.Stride);
                        int i;
                        for (int x = 0; x < bitmapDataInMemory.Width; x++) {
                            i = y * width + x;
                            row[x * PIXELSIZEINBYTE] = data[i];   //Blue  0-255
                            row[x * PIXELSIZEINBYTE + 1] = data[i]; //Green 0-255
                            row[x * PIXELSIZEINBYTE + 2] = data[i];   //Red   0-255
                            row[x * PIXELSIZEINBYTE + 3] = ALPHA;
                        }
                    }
                });
            }
            generatedBitmap.UnlockBits(bitmapDataInMemory);
            return generatedBitmap;
        }

        
        #endregion
    }

}
