﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AndorSDK {
    public static class CommandName {

        /// <summary>
        /// <para>Description: Starts an acquisition.</para>
        /// <para>Availability: Simcam, Neo, Zyla</para>
        /// </summary>
        public const string AcquisitionStart = "AcquisitionStart";

        /// <summary>
        /// <para>Description: Stops an acquisition.</para>
        /// <para>Availability: Simcam, Neo, Zyla</para>
        /// </summary>
        public const string AcquisitionStop = "AcquisitionStop";

        /// <summary>
        /// <para>Description: Dumps current hardware configuration information to file in the executable directory. File is called camdump-[Serial Number]</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string CameraDump = "CameraDump";

        /// <summary>
        /// <para>Description: Generates a software trigger in the camera. Used to generate each frame on the camera whenever the trigger mode is set to Software.</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string SoftwareTrigger = "SoftwareTrigger";

        /// <summary>
        /// <para>Description: Resets the camera’s internal timestamp clock to zero. As soon as the reset is complete the clock will begin incrementing from zero at the rate given by the TimestampClockFrequency feature.</para>
        /// <para>Availability: Neo,Zyla</para>
        /// </summary>
        public const string TimestampClockReset = "TimestampClock Reset";

    }
}
