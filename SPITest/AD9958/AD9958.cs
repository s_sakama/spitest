﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SPI;
using FTD2XX_SPI;

/* 
 * A library to control DDS using SPI.
 * The target synthesizer is AD9958.
 * According to https://github.com/WCP52/docs/wiki/Using-SPI-to-interact-with-the-AD9958-synthesizer ,
 * CPOL = 0;
 * CPHA = 1;
 * 
 * This is equivalent to Mode1
 * 
 * On my configuration,
 * CPOL = 1;
 * CPHA = 0;
 * AD9958 will read the state on falling edge of the SCLK, so this configuration should work.
 * 
 * Data is propagated (switched) on rising edge of SCLK
 * Data is read on falling edge of SCLK
 */

namespace DDS_SPI.AD9958
{
    class AD9958_SPI
    {
        //        FTDI_SPI ftdiDev;
        SPI_Master spiDevice;
        SPIConfig conf;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="ftdiDevice">
        /// FTDI device instance. MPSSE must be enabled beforehand.
        /// </param>
        public AD9958_SPI(SPI_Master spiDevice)
        {
            conf.bitOrder = SPIBitOrder.MSBFirst;
            //conf.mode = FTDI_SPI.SPIModeBuilder(true, false);
            conf.mode = SPIMode.mode2;
            this.spiDevice = spiDevice;

            this.spiDevice.InitChannel(conf);
        }
    }
}
