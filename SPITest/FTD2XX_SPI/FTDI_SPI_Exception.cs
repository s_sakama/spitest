﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FTD2XX_NET;

namespace FTD2XX_SPI.SPIException
{
    class DeviceNotFoundException : FTDI.FT_EXCEPTION
    {
        public DeviceNotFoundException()
        {

        }

        public DeviceNotFoundException(string message) : base(message)
        {

        }

        public DeviceNotFoundException(string message, Exception inner) : base(message, inner)
        {

        }
    }

    class DriverNotFoundException : FTDI.FT_EXCEPTION
    {
        public DriverNotFoundException()
        {

        }

        public DriverNotFoundException(string message) : base(message)
        {

        }

        public DriverNotFoundException(string message, Exception inner) : base(message, inner)
        {

        }
    }

    class DeviceSetupFailureException : FTDI.FT_EXCEPTION
    {
        public DeviceSetupFailureException()
        {
        }

        public DeviceSetupFailureException(string message) : base(message)
        {

        }

        public DeviceSetupFailureException(string message, Exception inner) : base(message, inner)
        {

        }
    }

    class SPICommunicationException : FTDI.FT_EXCEPTION
    {
        public SPICommunicationException()
        {
        }

        public SPICommunicationException(string message) : base(message)
        {

        }

        public SPICommunicationException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
