﻿#define FT232H

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Threading;

using SPI;
using SPI.SPI_AD9958;
using FTD2XX_NET;
using FT_BIT_MODES=FTD2XX_NET.FTDI.FT_BIT_MODES;
using FTD2XX_SPI.SPIException;

// SPI communication using MPSSE feature.
// All the data is sent in MSB first to DDS.
namespace FTD2XX_SPI
{
    class FTDI_SPI : SPI_AD9958, IDisposable
    {
        /* 
        * Pin assignment in LSB first
        */

        /* Lower bits (D0 ~ D7) */
        protected const byte SCLK = 0x01;
        protected const byte SDIO_0 = 0x02;
        protected const byte SDIO_2 = 0x04;
        protected const byte CSB = 0x08;
        protected const byte SDIO_3 = 0x10; // D4
        protected const byte IO_UPDATE = 0x20; // D5
        protected const byte MASTER_RESET = 0x40; // D6
        protected const byte PWR_DWN_CTL = 0x80; //D7


        private FTDI.FT_STATUS ftStatus = FTDI.FT_STATUS.FT_OK;
        private FTDI ftdiDevice;
        private uint deviceCount = 0;

        private uint bytesWritten = 0;
        private uint bytesRead = 0;
        private uint rxQueue = 0;
        private byte[] sendBuffer = new byte[500];
        private byte[] recvBuffer = new byte[500];

        public byte OPCODE_W { get; private set; }
        public byte OPCODE_RW { get; private set; }
        public byte OPCODE_R { get; private set; }

        private byte clockInitialState = 0x00;

        private readonly byte IOmodeBits = 0x00; //0b010;

        public bool MpsseEnabled { get; private set; }


        public FTDI_SPI()
        {
            ftdiDevice = new FTDI();
            MpsseEnabled = false;
            // Initialization process
            try
            {
                ftStatus = ftdiDevice.GetNumberOfDevices(ref deviceCount);
            }
            catch(FTDI.FT_EXCEPTION e)
            {
                throw new DriverNotFoundException("Driver not loaded.",e);
            }

            ftStatus = ftdiDevice.OpenByIndex(0);

            if (ftStatus == FTDI.FT_STATUS.FT_OK)
            {
                ftdiDevice.Purge(FTDI.FT_PURGE.FT_PURGE_RX | FTDI.FT_PURGE.FT_PURGE_TX);
            }
            else
            {
                throw new DeviceNotFoundException("Device not found.");
            }

            // Start device initialization for SPI over MPSSE...
        }

        private void ConfigureMPSSE()
        {
            ftStatus = FTDI.FT_STATUS.FT_OK;
            ftStatus |= ftdiDevice.SetTimeouts(1000, 1000);
            ftStatus |= ftdiDevice.SetLatency(16);
            //ftStatus |= ftdiDevice.SetFlowControl(FTDI.FT_FLOW_CONTROL.FT_FLOW_RTS_CTS, 0x00, 0x00);
            ftStatus |= ftdiDevice.SetBitMode(0x00, FT_BIT_MODES.FT_BIT_MODE_RESET);
            //ftStatus |= ftdiDevice.SetBitMode(0x00, 0x02);         // MPSSE mode        
            ftStatus |= ftdiDevice.SetBitMode(0xFB, FT_BIT_MODES.FT_BIT_MODE_MPSSE);         //0xFB = configure AD1(DO) as input, others are output by LSB first ,0x02=MPSSE mode

            if (ftStatus != FTDI.FT_STATUS.FT_OK)
            {
                throw new DeviceSetupFailureException("Device configuration failed.");
            }

            sendBuffer[0] = 0x8A; // disable the /5 divider
            ftStatus = ftdiDevice.Write(sendBuffer, 1, ref bytesWritten);

            sendBuffer[0] = 0x86; // set clock divisor
            sendBuffer[1] = 0x57; // Lower bits
            sendBuffer[2] = 0x02; // Higher bits
            ftStatus = ftdiDevice.Write(sendBuffer, 3, ref bytesWritten);

            sendBuffer[0] = 0x80;
            sendBuffer[1] = 0x08;
            sendBuffer[2] = 0x0B;
            
            ftStatus = ftdiDevice.Write(sendBuffer, 3, ref bytesWritten);

            if (ftStatus != FTDI.FT_STATUS.FT_OK)
            {
                ftdiDevice.Close();
                throw new DeviceSetupFailureException("Set divider failed.");
            }

            MpsseEnabled = true;

        }

        public void CheckMPSSE()
        {
            MpsseEnabled = false;
            ConfigureMPSSE();
            // Using bad command detection, see if MPSSE is enabled.
            // Enable Internal loopback
            sendBuffer[0] = 0x84;
            ftStatus = ftdiDevice.Write(sendBuffer, 1, ref bytesWritten);

            ftStatus = ftdiDevice.GetRxBytesAvailable(ref rxQueue);
            if(rxQueue != 0)
            {
                ftdiDevice.SetBitMode(0x0, FTDI.FT_BIT_MODES.FT_BIT_MODE_RESET); // reset port
                ftdiDevice.Close();
                throw new DeviceSetupFailureException("MPSSE receive buffer should be empty");
            }

            // Send bad command
            sendBuffer[0] = 0xAB;
            ftStatus = ftdiDevice.Write(sendBuffer, 1, ref bytesWritten);

            if (ftStatus != FTDI.FT_STATUS.FT_OK)
            {
                ftdiDevice.Close();
                throw new DeviceSetupFailureException("Write failed.");
            }

            do
            {
                ftStatus = ftdiDevice.GetRxBytesAvailable(ref rxQueue);
            } while ((rxQueue == 0) && (ftStatus == FTDI.FT_STATUS.FT_OK));

            ftStatus = ftdiDevice.Read(recvBuffer, 2, ref bytesRead);

            bool commandEchoed = false;

            for(int i = 0; i < bytesRead; i++)
            {
                if((recvBuffer[i] == 0xFA) && (recvBuffer[i+1] == 0xAB))
                {
                    commandEchoed = true;
                    break;
                }
            }

            if (!commandEchoed)
            {
                ftdiDevice.Close();
                throw new DeviceSetupFailureException("Error in synchronizing the MPSSE");
            }

            // disable loopback
            sendBuffer[0] = 0x85;
            ftStatus = ftdiDevice.Write(sendBuffer, 1, ref bytesWritten);

            ftStatus = ftdiDevice.GetRxBytesAvailable(ref rxQueue);
            if (rxQueue != 0)
            {
                ftdiDevice.SetBitMode(0x0, FTDI.FT_BIT_MODES.FT_BIT_MODE_RESET); // reset port
                ftdiDevice.Close();
                throw new DeviceSetupFailureException("MPSSE receive buffer should be empty");
            }
            MpsseEnabled = true;
        }

        public void OpenChannel()
        {

        }

        public void SPITestFunction()
        {
            /* MPSSE CONFIGURATION */
            ftStatus = FTDI.FT_STATUS.FT_OK;
            ftStatus |= ftdiDevice.SetTimeouts(1000, 1000);
            ftStatus |= ftdiDevice.SetLatency(16);
            //            ftStatus |= ftdiDevice.SetFlowControl(FTDI.FT_FLOW_CONTROL.FT_FLOW_RTS_CTS, 0x00, 0x00);
            ftStatus |= ftdiDevice.SetFlowControl(FTDI.FT_FLOW_CONTROL.FT_FLOW_RTS_CTS, 0x00, 0x00);
            ftStatus |= ftdiDevice.SetBitMode(0x00, FT_BIT_MODES.FT_BIT_MODE_RESET); 
            ftStatus |= ftdiDevice.SetBitMode(0xFB, FT_BIT_MODES.FT_BIT_MODE_MPSSE);         //0xFB = configure AD1(DO) as input, others are output by LSB first ,0x02=MPSSE mode

            if (ftStatus != FTDI.FT_STATUS.FT_OK)
            {
                throw new DeviceSetupFailureException("Device configuration failed.");
            }

            sendBuffer[0] = 0x8A; // 0x8A disable the /5 divider
            ftStatus |= ftdiDevice.Write(sendBuffer, 1, ref bytesWritten);

            sendBuffer[0] = 0x86; // set clock divisor
            sendBuffer[1] = 0x31; // Lower bits
            sendBuffer[2] = 0x00; // Higher bits
            ftStatus |= ftdiDevice.Write(sendBuffer, 3, ref bytesWritten);

            // initialize pins
            sendBuffer[0] = 0x80;
            sendBuffer[1] = 0x08;
            sendBuffer[2] = 0xBB;

            ftStatus |= ftdiDevice.Write(sendBuffer, 3, ref bytesWritten);

            if (ftStatus != FTDI.FT_STATUS.FT_OK)
            {
                ftdiDevice.Close();
                throw new DeviceSetupFailureException("Set divider failed.");
            }

            /* SPI Part */
            ftStatus = FTDI.FT_STATUS.FT_OK;
            int bytesToWrite = 0;
            // initialize pins
            sendBuffer[bytesToWrite++] = 0x80;
            sendBuffer[bytesToWrite++] = 0x08;
            sendBuffer[bytesToWrite++] = 0x0B;

            ftStatus |= ftdiDevice.Write(sendBuffer, bytesToWrite, ref bytesWritten);
            bytesToWrite = 0;

            sendBuffer[bytesToWrite++] = 0x10; // OPCODE
            sendBuffer[bytesToWrite++] = 9; // lengthL
            sendBuffer[bytesToWrite++] = 0; // lengthH
            sendBuffer[bytesToWrite++] = 0xAA; // data1
            sendBuffer[bytesToWrite++] = 0xAA; //data2
            sendBuffer[bytesToWrite++] = 0xBC; //data2
            sendBuffer[bytesToWrite++] = 0xB8; //data2
            sendBuffer[bytesToWrite++] = 0xD9; //data2
            sendBuffer[bytesToWrite++] = 0xAA; // data1
            sendBuffer[bytesToWrite++] = 0xAA; //data2
            sendBuffer[bytesToWrite++] = 0xBC; //data2
            sendBuffer[bytesToWrite++] = 0xB8; //data2
            sendBuffer[bytesToWrite++] = 0xD9; //data2
            ftStatus |= ftdiDevice.Write(sendBuffer, bytesToWrite, ref bytesWritten);

            /*bytesToWrite = 0;
            sendBuffer[bytesToWrite++] = 0x87; //Flush command
            ftStatus |= ftdiDevice.Write(sendBuffer, bytesToWrite, ref bytesWritten);*/

            bytesToWrite = 0;

            sendBuffer[bytesToWrite++] = 0x10; // OPCODE
            sendBuffer[bytesToWrite++] = 9; // lengthL
            sendBuffer[bytesToWrite++] = 0; // lengthH
            sendBuffer[bytesToWrite++] = 0xAA; // data1
            sendBuffer[bytesToWrite++] = 0xAA; //data2
            sendBuffer[bytesToWrite++] = 0xBC; //data2
            sendBuffer[bytesToWrite++] = 0xB8; //data2
            sendBuffer[bytesToWrite++] = 0xD9; //data2
            sendBuffer[bytesToWrite++] = 0xAA; // data1
            sendBuffer[bytesToWrite++] = 0xAA; //data2
            sendBuffer[bytesToWrite++] = 0xBC; //data2
            sendBuffer[bytesToWrite++] = 0xB8; //data2
            sendBuffer[bytesToWrite++] = 0xD9; //data2
            ftStatus |= ftdiDevice.Write(sendBuffer, bytesToWrite, ref bytesWritten);

            /*bytesToWrite = 0;
            sendBuffer[bytesToWrite++] = 0x87; //Flush command
            ftStatus |= ftdiDevice.Write(sendBuffer, bytesToWrite, ref bytesWritten);*/

            bytesToWrite = 0;
            sendBuffer[bytesToWrite++] = 0x80;
            sendBuffer[bytesToWrite++] = 0x08;
            sendBuffer[bytesToWrite++] = 0x0B;
            ftStatus |= ftdiDevice.Write(sendBuffer, bytesToWrite, ref bytesWritten);
            if (ftStatus != FTDI.FT_STATUS.FT_OK)
            {
                ftdiDevice.Close();
                throw new Exception("Something happenned.");
            }

        }

        // change in rising edge (CPOL = 0, CPHA = 1 (mode1)) or (CPOL = 1, CPHA = 0 (mode2))
        public override void InitChannel(SPIConfig conf)
        {
            // See AN135 p.11
            Mode = conf.mode;

            if (conf.bitOrder == SPIBitOrder.MSBFirst)
            {
                switch (Mode)
                {
                    case SPIMode.mode0:
                        clockInitialState = 0x00;
                        OPCODE_W = 0x11;
                        break;
                    case SPIMode.mode1:
                        clockInitialState = 0x00;
                        // may cause unexpected behaviour
                        OPCODE_W = 0x10;
                        break;
                    case SPIMode.mode2:
                        clockInitialState = 0x01;
                        OPCODE_W = 0x10;
                        break;
                    case SPIMode.mode3:
                        clockInitialState = 0x01;
                        // may cause unexpected behaviour
                        OPCODE_W = 0x11;
                        break;
                }
            } else
            {
                switch (Mode)
                {
                    case SPIMode.mode0:
                        clockInitialState = 0x00;
                        OPCODE_W = 0x19;
                        break;
                    case SPIMode.mode1:
                        clockInitialState = 0x00;
                        OPCODE_W = 0x18;
                        break;
                    case SPIMode.mode2:
                        clockInitialState = 0x01;
                        OPCODE_W = 0x18;
                        break;
                    case SPIMode.mode3:
                        clockInitialState = 0x01;
                        OPCODE_W = 0x19;
                        break;
                }
            }

            ConfigureMPSSE();

            int bytesToWrite = 0;
            sendBuffer[bytesToWrite++] = 0x80;
            sendBuffer[bytesToWrite++] = (byte) (CSB | clockInitialState); // put all pins to GND state (except CS, CS is pulled high)
            sendBuffer[bytesToWrite++] = 0x0B;

            ftStatus = ftdiDevice.Write(sendBuffer, bytesToWrite, ref bytesWritten);

            if (ftStatus != FTDI.FT_STATUS.FT_OK)
            {
                ftdiDevice.Close();
                throw new DeviceSetupFailureException("Write failed.");
            }

        }

        public override void CloseChannel()
        {
            throw new NotImplementedException();
        }

        public override void Read()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Send bytes on single operation.
        /// </summary>
        /// <param name="dataToTransfer"></param>
        /// <param name="bytesToTransfer"></param>
        /// <param name="bytesTransfered"></param>
        public override void Write(byte[] dataToTransfer, int bytesToTransfer, ref uint bytesTransfered)
        {
            byte lengthL = (byte)((bytesToTransfer - 1) & 0xFF);
            byte lengthH = (byte)(((bytesToTransfer - 1) >> 4) & 0xFF);
            uint bytesToWrite = 0;

            sendBuffer[bytesToWrite++] = 0x80;
            sendBuffer[bytesToWrite++] = (byte)(0x00 | clockInitialState);
            sendBuffer[bytesToWrite++] = 0x0B;

            ftStatus = ftdiDevice.Write(sendBuffer, 3, ref bytesWritten);

            if (ftStatus != FTDI.FT_STATUS.FT_OK)
            {
                ftdiDevice.Close();
                throw new SPICommunicationException("Initialization Failed.");
            }

            bytesToWrite = 0;

            sendBuffer[bytesToWrite++] = OPCODE_W;
            sendBuffer[bytesToWrite++] = lengthL;
            sendBuffer[bytesToWrite++] = lengthH;

            uint prefixLength = bytesToWrite;
            int transferIndex = 0;

            int bytesTransferable = Math.Min(sendBuffer.Length, (int) (bytesToTransfer+bytesToWrite));

            for(uint i = bytesToWrite; i < bytesTransferable; i++)
            {
                sendBuffer[bytesToWrite++] = dataToTransfer[transferIndex];
                transferIndex++;
            }

            ftStatus = ftdiDevice.Write(sendBuffer, bytesToWrite, ref bytesWritten);
            bytesTransfered = bytesWritten - prefixLength;

            // Assuming all the bytes will be sent within the interval of USB packet (125 us).
            
            /*uint buffersLeft = bytesTransfered;

            while(buffersLeft > 0)
            {
                ftdiDevice.GetTxBytesWaiting(ref buffersLeft);
            }*/

            ftStatus = ftdiDevice.Write(new byte[] { 0x87}, 1, ref bytesWritten);

            ftStatus = ftdiDevice.Write(new byte[] { 0x80, 0x08, 0x0B }, 3, ref bytesWritten);


            if (ftStatus != FTDI.FT_STATUS.FT_OK)
            {
                ftdiDevice.Close();
                throw new SPICommunicationException("Data transfer failed.");
            }
        }

        private void MasterReset()
        {
            int bytesToWrite = 0;
            sendBuffer[bytesToWrite++] = 0x80;
            sendBuffer[bytesToWrite++] = (byte)(MASTER_RESET);
            sendBuffer[bytesToWrite++] = 0xFF;

            ftdiDevice.Write(sendBuffer, bytesToWrite, ref bytesWritten);

            bytesToWrite = 0;

            sendBuffer[bytesToWrite++] = 0x80;
            sendBuffer[bytesToWrite++] = 0x00;
            sendBuffer[bytesToWrite++] = 0xFF;

            ftdiDevice.Write(sendBuffer, bytesToWrite, ref bytesWritten);
        }

        protected override void SetInstructionByte()
        {
            int bytesToWrite = 0;

            // InitializePins.
            sendBuffer[bytesToWrite++] = 0x80;
            sendBuffer[bytesToWrite++] = (byte)(0x00 | clockInitialState);
            sendBuffer[bytesToWrite++] = 0xFB;

            /* 
             * Send Instruction byte in MSB first. 
             */

            // R/W = low -> Write operation
            sendBuffer[bytesToWrite++] = OPCODE_W;

            // lengthL
            sendBuffer[bytesToWrite++] = 0x01;
            // lengthH
            sendBuffer[bytesToWrite++] = 0x00;

            // Write command to Addr = 0x00 (CSR : Chip select register);
            sendBuffer[bytesToWrite++] = 0x80;

            // Disable LSB first, Enable Disable Ch0 and Ch1 (This is sent in MSB first), single bit -serial (3 wire)
            sendBuffer[bytesToWrite++] = 0x00 | 0b010;

            /* Instruction byte End */

            ftdiDevice.Write(sendBuffer, bytesToWrite, ref bytesWritten);

            bytesToWrite = 0;

            /* Send IO Update */
            sendBuffer[bytesToWrite++] = 0x80;
            sendBuffer[bytesToWrite++] = (byte)(0x00 | IO_UPDATE | clockInitialState);
            sendBuffer[bytesToWrite++] = 0xFB;

            ftdiDevice.Write(sendBuffer, bytesToWrite, ref bytesWritten);

            bytesToWrite = 0;
            /* IO Update sent */
        }

        protected override void SetFR1(byte freqMultiplier)
        {
            int bytesToWrite = 0;
            /* Initialize pins for next command */
            sendBuffer[bytesToWrite++] = 0x80;
            sendBuffer[bytesToWrite++] = (byte)(0x00 | clockInitialState);
            sendBuffer[bytesToWrite++] = 0xFB;

            /* 
             * Configure function register 1 
             */

            sendBuffer[bytesToWrite++] = OPCODE_W;

            // lengthL
            sendBuffer[bytesToWrite++] = 0x03;
            // lengthH
            sendBuffer[bytesToWrite++] = 0x00;

            /* MSB first, FR1 addr = 0x01*/
            sendBuffer[bytesToWrite++] = 0x80 | 0x01;

            /* Set FR1:Function Register1 */

            /* [23:16]
             * Charge pump control = 0x00; (75 uA)
             * PLL divider ratio = clockMultiplier.
             * VCO gain control = low range.
             */
            sendBuffer[bytesToWrite++] = (byte)(freqMultiplier << 2);

            /* [15:8]
             * Profile pin configuration = 0b000 (not used)
             * Ramp-up/Ramp-down = 0b00 (disabled)
             * Modulation level = 0b00 (not used)
             */
            sendBuffer[bytesToWrite++] = 0x00;
            /* [7:0]
             * refclk input power-down = 0b0 (ref clk circuit enabled)
             * ext. power down mode = 0b0;
             * dac ref power-down = 0b0 (ref circuit enabled)
             * Manual hard sync = 0b00 (disabled)
             */
            sendBuffer[bytesToWrite++] = 0x00;

            ftdiDevice.Write(sendBuffer, bytesToWrite, ref bytesWritten);

            bytesToWrite = 0;

            /* Send IO Update */
            sendBuffer[bytesToWrite++] = 0x80;
            sendBuffer[bytesToWrite++] = (byte)(0x00 | IO_UPDATE | clockInitialState);
            sendBuffer[bytesToWrite++] = 0xFB;

            ftdiDevice.Write(sendBuffer, bytesToWrite, ref bytesWritten);

            bytesToWrite = 0;
            /* IO Update sent */
        }

        protected override void SetFR2()
        {
            int bytesToWrite = 0;
            /* Initialize pins for next command */
            sendBuffer[bytesToWrite++] = 0x80;
            sendBuffer[bytesToWrite++] = (byte)(0x00 | clockInitialState);
            sendBuffer[bytesToWrite++] = 0xFB;

            /* 
             * Configure function register 1 
             */

            sendBuffer[bytesToWrite++] = OPCODE_W;

            // lengthL
            sendBuffer[bytesToWrite++] = 0x02;
            // lengthH
            sendBuffer[bytesToWrite++] = 0x00;

            /* MSB first, FR2 addr = 0x02*/
            sendBuffer[bytesToWrite++] = 0x00 | 0x02;

            /* Set FR1:Function Register2 */

            /* [15:8]
             * Autoclear sweep accumulator = 0b0 
             * clear sweep accumulator = 0b0 
             * autoclear phase accumulator = 0b0
             */
            sendBuffer[bytesToWrite++] = 0x00;
            /* [7:0]
             * auto sync all 0b0
             */
            sendBuffer[bytesToWrite++] = 0x00;

            ftdiDevice.Write(sendBuffer, bytesToWrite, ref bytesWritten);

            bytesToWrite = 0;

            /* Send IO Update */
            sendBuffer[bytesToWrite++] = 0x80;
            sendBuffer[bytesToWrite++] = (byte)(0x00 | IO_UPDATE | clockInitialState);
            sendBuffer[bytesToWrite++] = 0xFB;

            ftdiDevice.Write(sendBuffer, bytesToWrite, ref bytesWritten);

            bytesToWrite = 0;
            /* IO Update sent */
        }

        public override void InitializeDDS(double refClk, byte freqMultiplier)
        {
            RefCLK = refClk;
            SysCLK = refClk * freqMultiplier;
            MasterReset();
            SetInstructionByte();
            SetFR1(freqMultiplier);
            SetFR2();

            // Now device is ready to set.
            int bytesToWrite = 0;
            sendBuffer[bytesToWrite++] = 0x80;
            sendBuffer[bytesToWrite++] = (byte)(0x00 | clockInitialState);
            sendBuffer[bytesToWrite++] = 0xFB;

            ftdiDevice.Write(sendBuffer, bytesToWrite, ref bytesWritten);        }

        protected override void SetFreqCh(byte ch, double freq)
        {
            int bytesToWrite = 0;

            // InitializePins.
            sendBuffer[bytesToWrite++] = 0x80;
            sendBuffer[bytesToWrite++] = (byte)(0x00 | clockInitialState);
            sendBuffer[bytesToWrite++] = 0xFB;

            /* 
             * Send Instruction byte in MSB first. 
             */

            // R/W = low -> Write operation
            sendBuffer[bytesToWrite++] = OPCODE_W;

            // lengthL
            sendBuffer[bytesToWrite++] = 0x01;
            // lengthH
            sendBuffer[bytesToWrite++] = 0x00;

            // Write command to Addr = 0x00 (CSR : Chip select register);
            sendBuffer[bytesToWrite++] = 0x80;

            byte chSelectBits;
            if (ch == 0x00)
            {
                chSelectBits = (byte)1 << 6;
            }
            else
            {
                chSelectBits = (byte)1 << 7;
            }

            // Disable LSB first, Enable Disable Ch0 and Ch1 (This is sent in MSB first), single bit -serial (3 wire)
            sendBuffer[bytesToWrite++] = (byte) (0x00 | chSelectBits | IOmodeBits);

            /* Instruction byte End */

            ftdiDevice.Write(sendBuffer, bytesToWrite, ref bytesWritten);

            bytesToWrite = 0;

            /* 
             * Set desired frequency
             */

            // R/W = low -> Write operation
            sendBuffer[bytesToWrite++] = OPCODE_W;

            // lengthL
            sendBuffer[bytesToWrite++] = 0x04;
            // lengthH
            sendBuffer[bytesToWrite++] = 0x00;

            // Write command to Addr = 0x04 (CFTW0 : Channel frequency tuning word.);
            sendBuffer[bytesToWrite++] = 0x04 | 0x80;

            // f_OUT = (FTW)*SYSCLK/2^32
            uint ftw = (uint) (freq / SysCLK * (1 << 32));
            outputFreq[ch] = ftw * SysCLK / (1 << 32);

            sendBuffer[bytesToWrite++] = (byte)((ftw >> 24) & (0xFFFF));
            sendBuffer[bytesToWrite++] = (byte)((ftw >> 16) & (0xFFFF));
            sendBuffer[bytesToWrite++] = (byte)((ftw >> 8) & (0xFFFF));
            sendBuffer[bytesToWrite++] = (byte)(ftw & (0xFFFF));

            ftdiDevice.Write(sendBuffer, bytesToWrite, ref bytesWritten);
        }

        public override void SetFrequency(double ch0, double ch1)
        {
            SetFreqCh(0, ch0);
            SetFreqCh(1, ch1);

            int bytesToWrite = 0;

            /* Send IO Update */
            sendBuffer[bytesToWrite++] = 0x80;
            sendBuffer[bytesToWrite++] = (byte)(0x00 | IO_UPDATE | clockInitialState);
            sendBuffer[bytesToWrite++] = 0xFB;

            ftdiDevice.Write(sendBuffer, bytesToWrite, ref bytesWritten);

            bytesToWrite = 0;
            /* IO Update sent */
        }

        public void Blink(int times, int millisec)
        {
            Stopwatch sw = new Stopwatch();
            long msLast = 0;
            sw.Start();
            ftStatus = ftdiDevice.Write(new byte[] { 0x80, 0x00, 0xFF }, 3, ref bytesWritten);
            while ((sw.ElapsedMilliseconds - msLast) < 1000) { }
            msLast = sw.ElapsedMilliseconds;

            for (int i = 0; i < times; i++)
            {
                ftStatus = ftdiDevice.Write(new byte[] { 0x80, 0x03, 0xFF }, 3, ref bytesWritten);
                while ((sw.ElapsedMilliseconds - msLast) < millisec) { }
                msLast = sw.ElapsedMilliseconds;
                ftStatus = ftdiDevice.Write(new byte[] { 0x80, 0x00, 0xFF }, 3, ref bytesWritten);
                while ((sw.ElapsedMilliseconds - msLast) < millisec) { }
                msLast = sw.ElapsedMilliseconds;
            }
            sw.Stop();
        }
        #region IDisposable Support
        private bool disposedValue = false; // 重複する呼び出しを検出するには

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: マネージ状態を破棄します (マネージ オブジェクト)。
                    ftdiDevice.Close();
                }

                // TODO: アンマネージ リソース (アンマネージ オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。

                disposedValue = true;
            }
        }

        // TODO: 上の Dispose(bool disposing) にアンマネージ リソースを解放するコードが含まれる場合にのみ、ファイナライザーをオーバーライドします。
        // ~FTDI_SPI() {
        //   // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
        //   Dispose(false);
        // }

        // このコードは、破棄可能なパターンを正しく実装できるように追加されました。
        public void Dispose()
        {
            // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
            Dispose(true);
            // TODO: 上のファイナライザーがオーバーライドされる場合は、次の行のコメントを解除してください。
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
